#pragma once

#include "Area.h"

using namespace std;

class DevelopmentalNetwork
{
public:
	DevelopmentalNetwork(int xAreas, int yAreas, int zAreas, bool enableModulation);
	virtual ~DevelopmentalNetwork() = default;

	vector<area_ptr> X, Xp, Xs, Y, Yp, Ys, RN, VTA, Z, Zpain, Zpleasure;

	void Update(vector<cv::Mat> lastSensoryPattern, vector<cv::Mat> lastEffectorPattern, vector<cv::Mat> effectorPattern, bool yFrozen, bool zFrozen, bool zSupervised, cv::Mat painPattern, cv::Mat pleasurePattern);
	void Setup(vector<int> xNumNeurons, int yNumNeurons, vector<int> zNumNeurons, int xResSize, int yResSize, vector<int> zResSize, bool debug = false);

	const int MODULATION_NEURONS = 9;
	void UpdateRN(mat& painPattern);
	void UpdateVTA(mat& pleasurePattern);
	void UpdateY(vector<cv::Mat>& sensoryPattern, vector<cv::Mat>& lastEffectorPattern, bool yFrozen);
	void UpdateZ(vector<cv::Mat>& effectorPattern, bool zFrozen, bool zSupervised);
	void Modulate();

private:
	void SetupX();
	void SetupY();
	void SetupZ();
	bool modulation = true;
};