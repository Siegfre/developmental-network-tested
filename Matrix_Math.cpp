#include "Matrix_Math.h"

void Matrix_Math::Normalize(cv::Mat input, cv::Mat output)
{
	double length = cv::norm(input, cv::NormTypes::NORM_L2);
	cv::divide(input, length, output);
}

void Matrix_Math::Normalize(mat input, mat& output)
{
	output = normalise(input, 2);
}

cv::Mat Matrix_Math::FlipMatrix(cv::Mat input)
{
	return cv::Mat::ones(input.rows, input.cols, CV_32F) - input;
}

mat Matrix_Math::FlipMatrix(mat input)
{
	return mat(input.n_rows, input.n_cols, fill::ones) - input;
}

mat Matrix_Math::FromCvMat(cv::Mat input)
{
	cv::Mat tempMat;
	input.convertTo(tempMat, CV_64F);
	return mat(reinterpret_cast<const double*>(tempMat.data), tempMat.rows, tempMat.cols);
}

cv::Mat Matrix_Math::FromArmaMat(mat input)
{
	cv::Mat tempMat = cv::Mat(input.n_rows, input.n_cols, CV_64F, input.memptr());
	tempMat.convertTo(tempMat, CV_32F);
	return tempMat;
}