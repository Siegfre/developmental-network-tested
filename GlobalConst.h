#pragma once

// REFERENCES
// [0] OPTIMALITY AND HIERARCHICAL REPRESENTATION IN EMERGENT NEURAL TURING MACHINES AND THEIR VISUAL  NAVIGATION - ProQuest Number: 10789741
// [1] CORTEX-INSPIRED DEVELOPMENTAL LEARNING NETWORKS FOR STEREO VISION
// [2] Brain-Like Learning Directly from Dynamic Cluttered Natural Video
// [3] Stereo Where-What Networks: Unsupervised Binocular Feature Learning Mojtaba Solgi Member, IEEE and Juyang Weng, Fellow, IEEE// [4] Modulation for emergent networks: Serotonin and dopamine - 10.1016/j.neunet.2012.11.008

#define PERFECT_MATCH 1.0
#define BASE_MITOSIS_MODIFIER 0.5
/* **WITHIN DOMAIN SYNAPTIC MAINTENANCE** */
/* A pixel input between 0.0 and 1.0, each pixel contains 256 unique values.
This value is dependent on the type of input, and may be different for different areas.
It serves as an estimate of 8-bit resolution of response in the range [0,1] 
It serves to avoid a too large synaptogenic factor and synaptic deviation? [2] */
#define DIGITAL_NOISE (1.0 / 256.0)
// The constant standard deviation that all neurons start with, representing the standard deviation of uniform distribution in [-digital noise, digital noise]
// it is supposed to represent a good starting value, not sure why though.
// ALTERNATE VALUES:
// [0] machine zero (DBL_EPSILON) is used
// [1] 0 is used
#define CONSTANT_STANDARD_DEVIATION (DIGITAL_NOISE / sqrt(12))
// [0] has these values not as constants but as variables that change over time
#define BETA_S 1.0 // Alternate values [0] 0.8
#define BETA_S1 0.9 // Alternate values from [1] used in DSLCA
#define BETA_B 1.5
// Some constant used in DSLCA to ensure that the dynamic trimming function is continuous, not clear what this value should be. No example provided.
// Coulomb's constant? Boltzmann constant? Some random constant? Currently set to a constant that will leave the synaptogenic factor smaller then
// when the synapse is being grown or being cut
#define CONTINUOUS_K 0.5
// Synaptic maintenance occurs after this neuron age
// Until this age the constant starting deviation will be used and as a result no synaptic deviation will occur
// neuron.age < WAITING_LATENCY then do not perform synaptic maintenance
// neuron.age > WAITING_LATENCY then perform synaptic maintenance
#define WAITING_LATENCY_BASIC 80 // [3] pg 3
#define WAITING_LATENCY_CUTTING 80 // [1] the source of the term, [3] use in the final algorithm as "nc"
#define WAITING_LATENCY_GROWING 30 // [1] the source of the term, [3] use in the final algorithm as "ng"
// **WITHIN DOMAIN SYNAPTIC MAINTENANCE**
#define DEFAULT_T1 80
#define DEFAULT_T2 200
#define REFRESH_RATE 2000
// c is the learning rate increase over normal average if c is 2, each new sample will be weighted from 2 to 3 times more in the second interval.
// The learning rate at the second threshold.
#define LEARNING_RATE_AUGMENTATION 2
#define PAIN_MODIFIER 3.0 // [4] Is used to augment the Z response in a motivated network
#define PLEASURE_MODIFIER 1.0 // [4] Is used to augment the Z response in a motivated network
#define RESPONSE_UNDER_PAIN 0.25
#define LEARNING_BOOST 1.0
#define ALL_REGION_COMBINATIONS 7
#define NEARBY_NEURON_COUNT 6
#define ENABLE_MODULATION true
#define DISABLE_MODULATION false
#define DISTANCE_BETWEEN_GLIAL_CELLS 1 // This is the shortest distance between two glial cells (3D points)
// These minimum thresholds were determined by manual adjustment and represent the values necessary at 12-MAR-19
// needed to pass all tests in particular to pass the limited resources test while still passing all other tests
#define MINIMUM_X_FIRING_THRESHOLD 0.01
#define MINIMUM_Y_FIRING_THRESHOLD 0.15
#define MINIMUM_Z_FIRING_THRESHOLD 0.20
#define SIGNIFICANT_PRESYNAPTIC_ACTIVITY 0.50 // When converting an initial state neuron into a learning state neuron (WWN-7), this value determines the minimum pre-synaptic activity required.