#include "Neuron.h"
#include "catch.hpp"
#include "Matrix_Math.h"

// REFERENCES
// [0] Stereo Where-What Networks: Unsupervised Binocular Feature Learning Mojtaba Solgi Member, IEEE and Juyang Weng, Fellow, IEEE// [1] CORTEX-INSPIRED DEVELOPMENTAL LEARNING NETWORKS FOR STEREO VISION// [1 - Note] Some equations seem to be written wrong, this version may be unreliable or perhaps the person writing this has an old pre-print// [2] Information Dense Actions as Contexts - 10.1016/j.neucom.2018.05.056
// [3] WWN-9: Cross-domain synaptic maintenance and its application to object groups recognition - 10.1109/ijcnn.2014.6889960
// [4] Neuron-wise Inhibition Zones and Auditory Experiments - 10.1109/TIE.2019.2891400

void Neuron::Clear()
{
	response.Clear();

	if (HasSiblings())
	{
		pleasureSibling->response.Clear();
		painSibling->response.Clear();
	}
}

double Neuron::amnesicMean(double age, int t1, int t2, int c, int r)
{
	double u = 0;

	if (age < t1)
		u = 0;
	else if (t1 < age && age < t2)
		u = c * (age - t1) / (t2 - t1);
	else
		u = c + (age - t2) / r;

	return u;
}

bool Neuron::HasSiblings()
{
	return painSibling != nullptr && pleasureSibling != nullptr;
}

// Update the weight vectors using Hebbian-like learning
void Neuron::UpdateWeights(bool trim, double painValue, double pleasureValue)
{
	// [2] "u" prevents w2 from converging to zero as the age increases
	const double u = amnesicMean(age, t1, t2, LEARNING_RATE_AUGMENTATION, REFRESH_RATE);

	// Update weight vectors for matched neuron using the amnesic mean
	// w1 + w2 = 1
	const double w2 = (1.0 + u) / age; // Learning rate
	const double w1 = 1.0 - w2; // Retention rate

	CompactPattern();
	HebbianLearning(weights, w1, w2, response.postSynaptic, foldedInput);
	TrimSynapses(synapticAges, trimmedInput, weights, synapticDeviation, synaptogenicFactor, trimmedWeights, trim);

	// Only learning state neurons increase their age, this process is described in WWN-7
	if (initialization == LEARNING)
	{
		//The age of the winner neuron is incremented by the learned amount, if updating non-lateral connections.
		age = age + response.postSynaptic;
	}
}

void Neuron::SynapseMaintenance(mat &input, const mat &synaptogenicFactor)
{
	const bool containsNonZeroValue = any(vectorise(input));

	if (containsNonZeroValue)
		input = input % synaptogenicFactor; // We use the result in the dot so there is no reason to normalize here
}

void Neuron::TrimSynapses()
{
	SynapseMaintenance(trimmedInput, synaptogenicFactor);
}

void Neuron::HebbianLearning(mat& weights, double w1, double w2, double preresponse, const mat &noInput)
{
	// Hebbian learning - If updating a lateral connection, scale the pre-response value.
	weights = (weights * w1) + (w2 * (noInput * preresponse)); // Vj = (W1 * Vj) + (W2 * (yP)) - NAI Pg. 239
}

void Neuron::SetModulationSiblings(Neuron* pain, Neuron* pleasure)
{
	painSibling = pain;
	pleasureSibling = pleasure;
}

mat Neuron::ComputeSynaptogenicFactor(mat &synapticAges, mat &synapticDeviation, double averageSynapticVariance, mat synaptogenicFactor)
{
	// An implementation of DSLCA (Dynamic Synapse Lobe Component Analysis) [0], [1] pg. 76 & 77
	// Allows for autonomous retraction and growth of synapses during the neural learning process
	// this is in contrast with the "static" synapse maintenance that only allows for retration of synapses
	// this algorithm was designed with the assumption of binocular receptive fields (stereo vision), it is unclear if it is generally applicable
	if (SYNAPTOGENIC_TYPE == DYNAMIC)
	{
		const int totalDistance = 3;
		const int maxTries = pow(totalDistance, totalDistance);
		int rowRand = 0, colRand = 0;

		for (int i = 0; i < synapticDeviation.n_rows; i++)
		{
			for (int j = 0; j < synapticDeviation.n_cols; j++)
			{
				const double relativeSynapticDeviation = synapticDeviation(i, j) / averageSynapticVariance;

				if (relativeSynapticDeviation >= BETA_S1 && relativeSynapticDeviation <= BETA_B)
					synaptogenicFactor(i, j) = (BETA_B - relativeSynapticDeviation) / (BETA_B - BETA_S1); // Scale synapse
				else if (relativeSynapticDeviation < BETA_S1)
				{
					synaptogenicFactor(i, j) = 1.0;

					const bool growNewConnection = (static_cast<int>(age) % WAITING_LATENCY_GROWING) == 0;

					if (growNewConnection)
					{
						for (int tries = 0; tries < maxTries; tries++)
						{
							rowRand = (rand() % synapticDeviation.n_rows);
							colRand = (rand() % synapticDeviation.n_cols);

							if (synaptogenicFactor.in_range(rowRand, colRand) && synaptogenicFactor(rowRand, colRand) == 0.0)
							{
								synapticAges(rowRand, colRand) = age;
								synaptogenicFactor(rowRand, colRand) = 1.0;
								break;
							}
						}
					}
				}
				else if (relativeSynapticDeviation > BETA_B)
					synaptogenicFactor(i, j) = 0.0; // Do not replace this with EPSILON, the connections need to be fully cut for proper function.
			}
		}
	}

	return synaptogenicFactor;
}

void Neuron::CompactPattern(mat &input, mat &output)
{
	const int pattern_width = output.n_rows;
	const int pattern_height = output.n_cols;

	mat meanInput = mat(pattern_width, pattern_height, fill::zeros);

	for (int i = 0; i < input.n_rows; i++)
	{
		for (int j = 0; j < input.n_cols; j++)
		{
			if (input.in_range(i * pattern_width, j * pattern_height, size(pattern_width, pattern_height)))
				meanInput += input.submat(i * pattern_width, j * pattern_height, size(pattern_width, pattern_height));
		}
	}

	int totalPatterns = (input.n_rows / pattern_width) * (input.n_cols / pattern_height);

	meanInput /= totalPatterns;
	output = meanInput;
}

void Neuron::UnfoldPattern(mat &input, mat &output)
{
	const int pattern_width = input.n_rows;
	const int pattern_height = input.n_cols;

	for (int i = 0; i < output.n_rows; i++)
	{
		for (int j = 0; j < output.n_cols; j++)
		{
			if (output.in_range(i * pattern_width, j * pattern_height, size(pattern_width, pattern_height)))
				output.submat(i * pattern_width, j * pattern_height, size(pattern_width, pattern_height)) = input;
		}
	}
}

void Neuron::ExpandPattern()
{
	UnfoldPattern(weights, unfoldedWeights);
}

void Neuron::CompactPattern()
{
	CompactPattern(input, foldedInput);
}

double Neuron::TrimSynapses(mat &synapticAge, const mat& noInput, const mat &weights, mat &synapticDeviation, mat &synaptogenicFactor, mat &trimmedWeights, bool trim)
{
	if (trim)
	{
		double expectedSynapticDeviation = 0.0;

		for (int i = 0; i < synapticDeviation.n_rows; i++)
		{
			for (int j = 0; j < synapticDeviation.n_cols; j++)
			{
				// [4] - Calculates w1 and w2 with the delta between the neurons age and the synapse's age
				int adjustedAge = age - synapticAge(i, j);
				if (adjustedAge <= WAITING_LATENCY_BASIC)
				{
					synapticDeviation(i, j) = CONSTANT_STANDARD_DEVIATION;
					expectedSynapticDeviation += synapticDeviation(i, j);
				}
				else if (adjustedAge > WAITING_LATENCY_BASIC)
				{
					// Subtract the minimum waiting latency to ensure that the synapticDeviation's learning rate starts at 1.0
					adjustedAge -= WAITING_LATENCY_BASIC;
					// If this ends up slowing things down too much either start caching common values or remove it.
					// The difference in performance seems negligible for our relatively small datasets
					double u = amnesicMean(adjustedAge, t1, t2, LEARNING_RATE_AUGMENTATION, REFRESH_RATE);
					double w2 = (1.0 + u) / adjustedAge; // Learning rate
					double w1 = 1.0 - w2; // Retention rate
					/* CSE-TechReport-12-5 Pg.8 - "Here, Mahalanobis distance between the normalized input vector p and the synaptic vector v is used
					instead of the Euclidean distance." */
					synapticDeviation(i, j) = max((w1 * synapticDeviation(i, j)) + (w2 * abs(weights(i,j) - noInput(i,j))), CONSTANT_STANDARD_DEVIATION); // Take the max to ensure a non-zero value for synapticDeviation
					expectedSynapticDeviation += synapticDeviation(i, j);
				}
			}
		}

		expectedSynapticDeviation = expectedSynapticDeviation / synapticDeviation.size(); // Average deviation among all the synapses of the neuron

		synaptogenicFactor = ComputeSynaptogenicFactor(synapticAge, synapticDeviation, expectedSynapticDeviation, synaptogenicFactor);

		mat noWeights = mat(static_cast<const double*>(weights.begin()), weights.n_rows, weights.n_cols);
		noWeights = noWeights % synaptogenicFactor;

		Matrix_Math::Normalize(noWeights, trimmedWeights);

		return expectedSynapticDeviation;
	}
	else
		Matrix_Math::Normalize(weights, trimmedWeights);

	return DBL_EPSILON;
}

double Neuron::CalculatePreresponse(const mat& inputs, const mat& weights)
{
	double result = 0.0;
	double similarity = 0;

	if (weights.size() < inputs.size())
		similarity = norm_dot(inputs, weights);
	else
	{
		mat normedInput = mat(static_cast<const double*>(inputs.begin()), inputs.n_rows, inputs.n_cols);
		Matrix_Math::Normalize(inputs, normedInput);
		similarity = norm_dot(normedInput, weights);
	}

	if (similarity != 0 && !isnan(similarity))
		result += (similarity / inputs.size());

	return result;
}

double Neuron::round_decimal(double input, int places)
{
	double threshhold = pow(10.0, places);
	return round(threshhold * input) / threshhold;
}

double Neuron::CalculatePreresponse()
{
	ExpandPattern();

	const int width = weights.n_rows;
	const int height = weights.n_cols;

	vector<double> perPatchPreResponses = vector<double>();
	mat matWeightedAverage = mat(width, height);
	double totalPreResponse = 0.0;
	for (int x = 0; x < trimmedInput.n_rows; x++)
	{
		for (int y = 0; y < trimmedInput.n_cols; y++)
		{
			if (trimmedInput.in_range(x * width, y * height, size(width, height))) // TODO: Do this better, checking if its in_range should be unnecessary
			{
				mat submat = trimmedInput.submat(x * width, y * height, size(width, height));
				const double preResponse = CalculatePreresponse(submat, weights);
				perPatchPreResponses.push_back(preResponse);
				matWeightedAverage += (preResponse * submat);
				totalPreResponse += preResponse;
			}
			else
				break;
		}
	}
	totalPreResponse /= perPatchPreResponses.size();
	matWeightedAverage /= perPatchPreResponses.size();

	return CalculatePreresponse(trimmedInput, unfoldedWeights);
}