#pragma once

// REFERENCES
// [0] Actions as Contexts
// [1] CORTEX-INSPIRED DEVELOPMENTAL LEARNING NETWORKS FOR STEREO VISION

enum REGION_COMBINATIONS
{
	X,
	Y,
	Z,
	XY,
	XZ,
	YZ,
	XYZ,
	UNINITIALIZED
};

enum AUGMENTATION
{
	POSITIVE,
	NEUTRAL,
	NEGATIVE
};

enum AREA
{
	FRONT = -1,
	LATERAL = 0,
	BACK = 1
};

enum TYPE_SYNAPTOGENIC_FACTOR
{
	HYBRID, // A combination of the STATIC and DYNAMIC algorithms
	DYNAMIC, // Calculate the synaptogenic factor using the DSLCA algorithm [1]
	STATIC // Calculate the synaptogenic factor using the standard LCA algorithm [0]
};

enum INPUT_TYPE
{
	TEMPORAL,
	SUPERVISED,
	PREVIOUSLY_TEMPORAL_BUT_CURRENTLY_UNDEFINED_FOR_TESTING
};

enum INITIALIZATION_STATE
{
	INITIAL,
	LEARNING
};