#include "catch.hpp"
#include "DevelopmentalNetwork.h"
#include "readMNIST.h"
#include <sstream>

TEST_CASE("Dopamine & Serotonin are diffused to correctly modify the w2 value", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 7, Y_NEURONS = 100;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 100 }, Z_NEURONS_PER_AREA = { 10 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, ENABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		vector<cv::Mat> sensoryPattern = { cv::Mat::ones(10, 10, CV_32F) };
		vector<cv::Mat> lastEffectorPattern = { cv::Mat::ones(10, 1, CV_32F) };

		WHEN("We manually perform an update, 1 pain, 0 pleasure")
		{
			mat painPattern = ones<mat>(10, 10);
			mat pleasurePattern = zeros<mat>(10, 10);

			network.UpdateRN(painPattern);
			network.UpdateVTA(pleasurePattern);

			network.UpdateY(sensoryPattern, lastEffectorPattern, false);

			THEN("Y areas return 1 for pain")
			{
				CHECK(network.Y[0]->CalculatePainValue() == 1.0);
				CHECK(network.Ys[0]->CalculatePainValue() == 1.0);
				CHECK(network.Yp[0]->CalculatePainValue() == 1.0);
			}			
			
			THEN("Y areas return 0 for pleasure")
			{
				CHECK(network.Y[0]->CalculatePleasureValue() == 0.0);
				CHECK(network.Ys[0]->CalculatePleasureValue() == 0.0);
				CHECK(network.Yp[0]->CalculatePleasureValue() == 0.0);
			}
		}

		WHEN("We manually perform an update, 0 pain, 1 pleasure")
		{
			mat painPattern = zeros<mat>(10, 10);
			mat pleasurePattern = ones<mat>(10, 10);

			network.UpdateRN(painPattern);
			network.UpdateVTA(pleasurePattern);

			network.UpdateY(sensoryPattern, lastEffectorPattern, false);

			THEN("Y areas return 1 for pain")
			{
				CHECK(network.Y[0]->CalculatePainValue() == 0.0);
				CHECK(network.Ys[0]->CalculatePainValue() == 0.0);
				CHECK(network.Yp[0]->CalculatePainValue() == 0.0);
			}

			THEN("Y areas return 0 for pleasure")
			{
				CHECK(network.Y[0]->CalculatePleasureValue() == 1.0);
				CHECK(network.Ys[0]->CalculatePleasureValue() == 1.0);
				CHECK(network.Yp[0]->CalculatePleasureValue() == 1.0);
			}
		}

		WHEN("We manually perform an update, 1 pain, 1 pleasure")
		{
			mat painPattern = ones<mat>(10, 10);
			mat pleasurePattern = ones<mat>(10, 10);

			network.UpdateRN(painPattern);
			network.UpdateVTA(pleasurePattern);

			network.UpdateY(sensoryPattern, lastEffectorPattern, false);

			THEN("Y areas return 1 for pain")
			{
				CHECK(network.Y[0]->CalculatePainValue() == 1.0);
				CHECK(network.Ys[0]->CalculatePainValue() == 1.0);
				CHECK(network.Yp[0]->CalculatePainValue() == 1.0);
			}

			THEN("Y areas return 0 for pleasure")
			{
				CHECK(network.Y[0]->CalculatePleasureValue() == 1.0);
				CHECK(network.Ys[0]->CalculatePleasureValue() == 1.0);
				CHECK(network.Yp[0]->CalculatePleasureValue() == 1.0);
			}
		}

		WHEN("We manually perform an update, 0 pain, 0 pleasure")
		{
			mat painPattern = zeros<mat>(10, 10);
			mat pleasurePattern = zeros<mat>(10, 10);

			network.UpdateRN(painPattern);
			network.UpdateVTA(pleasurePattern);

			network.UpdateY(sensoryPattern, lastEffectorPattern, false);

			THEN("Y areas return 1 for pain")
			{
				CHECK(network.Y[0]->CalculatePainValue() == 0.0);
				CHECK(network.Ys[0]->CalculatePainValue() == 0.0);
				CHECK(network.Yp[0]->CalculatePainValue() == 0.0);
			}

			THEN("Y areas return 0 for pleasure")
			{
				CHECK(network.Y[0]->CalculatePleasureValue() == 0.0);
				CHECK(network.Ys[0]->CalculatePleasureValue() == 0.0);
				CHECK(network.Yp[0]->CalculatePleasureValue() == 0.0);
			}
		}
	}
}

TEST_CASE("Synapse maintenance test", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 3;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on a single type of input")
		{
			for (int i = 0; i < 1000; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				cv::randn(lastSensoryPattern, cv::Scalar(0.0), cv::Scalar(1.0));
				lastSensoryPattern.at<float>(0) = 1.0f;
				lastSensoryPattern.at<float>(1) = 0.0f;
				lastSensoryPattern.at<float>(2) = 1.0f;
				lastSensoryPattern.at<float>(3) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				cv::randn(lastEffectorPattern, cv::Scalar(0.0), cv::Scalar(1.0));
				lastEffectorPattern.at<float>(0) = 1.0f;
				lastEffectorPattern.at<float>(1) = 0.0f;
				lastEffectorPattern.at<float>(2) = 1.0f;
				lastEffectorPattern.at<float>(3) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				cv::randn(curEffectorPattern, cv::Scalar(0.0), cv::Scalar(1.0));
				curEffectorPattern.at<float>(0) = 1.0f;
				curEffectorPattern.at<float>(1) = 0.0f;
				curEffectorPattern.at<float>(2) = 1.0f;
				curEffectorPattern.at<float>(3) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			THEN("Check to see that the stable connections contain no synaptic cuts and unstable connections are cut")
			{
				CHECK(network.Y[0]->getNeurons()[0].backSynaptogenicFactor[0](0) == 1.0);
				CHECK(network.Y[0]->getNeurons()[0].backSynaptogenicFactor[0](1) == 1.0);
				CHECK(network.Y[0]->getNeurons()[0].backSynaptogenicFactor[0](2) == 1.0);
				CHECK(network.Y[0]->getNeurons()[0].backSynaptogenicFactor[0](3) == 1.0);
				CHECK(network.Y[0]->getNeurons()[0].backSynaptogenicFactor[0](4) < 0.5);
				CHECK(network.Y[0]->getNeurons()[0].backSynaptogenicFactor[0](5) < 0.5);
				CHECK(network.Y[0]->getNeurons()[0].backSynaptogenicFactor[0](6) < 0.5);
				CHECK(network.Y[0]->getNeurons()[0].backSynaptogenicFactor[0](7) < 0.5);

				CHECK(network.Y[0]->getNeurons()[2].frontSynaptogenicFactor[0](0) == 1.0);
				CHECK(network.Y[0]->getNeurons()[2].frontSynaptogenicFactor[0](1) == 1.0);
				CHECK(network.Y[0]->getNeurons()[2].frontSynaptogenicFactor[0](2) == 1.0);
				CHECK(network.Y[0]->getNeurons()[2].frontSynaptogenicFactor[0](3) == 1.0);
				CHECK(network.Y[0]->getNeurons()[2].frontSynaptogenicFactor[0](4) < 0.5);
				CHECK(network.Y[0]->getNeurons()[2].frontSynaptogenicFactor[0](5) < 0.5);
				CHECK(network.Y[0]->getNeurons()[2].frontSynaptogenicFactor[0](6) < 0.5);
				CHECK(network.Y[0]->getNeurons()[2].frontSynaptogenicFactor[0](7) < 0.5);
			}
		}
	}
}

TEST_CASE("Z plasticity test", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 3;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		cv::Mat finalEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
		finalEffectorPattern.at<float>(4) = 1.0f;

		WHEN("We perform updates on a single type of input, we then attempt to form a new connection")
		{
			for (int i = 0; i < 50; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(0) = 1.0f;
				lastSensoryPattern.at<float>(1) = 1.0f;
				lastSensoryPattern.at<float>(2) = 1.0f;
				lastSensoryPattern.at<float>(3) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>(0) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			for (int i = 0; i < 50; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(0) = 1.0f;
				lastSensoryPattern.at<float>(1) = 1.0f;
				lastSensoryPattern.at<float>(2) = 1.0f;
				lastSensoryPattern.at<float>(3) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(0) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { finalEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			THEN("Check that the Z area has learned the new connection")
			{
				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
				CHECK(std::equal(effectorResult.begin<float>(), effectorResult.end<float>(), finalEffectorPattern.begin<float>()));
			}
		}
	}
}

TEST_CASE("Y bias test", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 3, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 20;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16, 16, 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);
		network.X[UNBIASED_INPUTS - 2]->augmentation = NEGATIVE;
		network.X[UNBIASED_INPUTS - 1]->augmentation = POSITIVE;

		cv::Mat finalEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
		finalEffectorPattern.at<float>(4) = 1.0f;

		WHEN("We perform updates to generate two unique input states, connected to two unique output states")
		{
			for (int i = 0; i < 50; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(0) = 1.0f;
				lastSensoryPattern.at<float>(1) = 1.0f;
				lastSensoryPattern.at<float>(2) = 1.0f;
				lastSensoryPattern.at<float>(3) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>(0) = 1.0f;

				cv::Mat lastAugmentedPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastAugmentedPattern.at<float>(0) = 1.0f;

				network.Update({ lastSensoryPattern, lastAugmentedPattern, lastAugmentedPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			for (int i = 0; i < 50; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(4) = 1.0f;
				lastSensoryPattern.at<float>(5) = 1.0f;
				lastSensoryPattern.at<float>(6) = 1.0f;
				lastSensoryPattern.at<float>(7) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(1) = 1.0f;

				cv::Mat lastAugmentedPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastAugmentedPattern.at<float>(0) = 1.0f;

				network.Update({ lastSensoryPattern, lastAugmentedPattern, lastAugmentedPattern }, { lastEffectorPattern }, { finalEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			THEN("Check that the Z area has learned the new connection")
			{
				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
				CHECK(std::equal(effectorResult.begin<float>(), effectorResult.end<float>(), finalEffectorPattern.begin<float>()));
			}

			cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
			lastSensoryPattern.at<float>(0) = 1.0f;
			lastSensoryPattern.at<float>(1) = 1.0f;
			lastSensoryPattern.at<float>(2) = 1.0f;
			lastSensoryPattern.at<float>(3) = 1.0f;

			cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
			lastEffectorPattern.at<float>(0) = 1.0f;

			cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
			curEffectorPattern.at<float>(0) = 1.0f;

			cv::Mat lastAugmentedPattern = cv::Mat::zeros(4, 4, CV_32F);
			lastAugmentedPattern.at<float>(0) = 1.0f;

			network.Update({ lastSensoryPattern, lastAugmentedPattern, lastAugmentedPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

			THEN("Check that the Z area still remembers the old connection")
			{
				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
				CHECK(std::equal(effectorResult.begin<float>(), effectorResult.end<float>(), curEffectorPattern.begin<float>()));
			}

			for (int i = 0; i < 50; i++)
			{
				lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(4) = 1.0f;
				lastSensoryPattern.at<float>(5) = 1.0f;
				lastSensoryPattern.at<float>(6) = 1.0f;
				lastSensoryPattern.at<float>(7) = 1.0f;

				cv::Mat lastPainPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastPainPattern.at<float>(1) = 1.0f;

				cv::Mat lastPleasurePattern = cv::Mat::zeros(4, 4, CV_32F);
				lastPleasurePattern.at<float>(0) = 1.0f;

				lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(1) = 1.0f;

				network.Update({ lastSensoryPattern, lastPainPattern, lastPleasurePattern }, { lastEffectorPattern }, { finalEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
				curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>(0) = 1.0f;

				if (std::equal(effectorResult.begin<float>(), effectorResult.end<float>(), curEffectorPattern.begin<float>()))
				{
					SUCCEED();
					return;
				}
			}

			FAIL();
		}
	}
}

TEST_CASE("Minimum Y representation test", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 32;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on a single type of input")
		{
			cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
			lastSensoryPattern.at<float>(0) = 1.0f;

			cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
			lastEffectorPattern.at<float>(0, 0) = 1.0f;

			cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
			curEffectorPattern.at<float>(1, 0) = 1.0f;

			network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

			THEN("Display the original pattern and get the next pattern back")
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(0) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(0, 0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>(1, 0) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
				cv::Mat internalResult = Matrix_Math::FromArmaMat(network.Y[0]->getPredictedResponse());

				CHECK(effectorResult.at<float>(1, 0) == 1.0);
			}
		}
	}
}

TEST_CASE("Minimum Y representation test multiple inputs", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 2, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 2;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 32;
		vector<int> MINIMUM_Z_RESPONSE = { 1, 1 }, X_NEURONS_PER_AREA = { 16, 16 }, Z_NEURONS_PER_AREA = { 16, 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on a single type of input")
		{
			cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
			lastSensoryPattern.at<float>(0) = 1.0f;

			cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
			lastEffectorPattern.at<float>(0, 0) = 1.0f;

			cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
			curEffectorPattern.at<float>(1, 0) = 1.0f;

			network.Update({ lastSensoryPattern, lastSensoryPattern }, { lastEffectorPattern, lastEffectorPattern }, { curEffectorPattern, curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

			THEN("Display the original pattern and get the next pattern back")
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(0) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(0, 0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>(1, 0) = 1.0f;

				network.Update({ lastSensoryPattern, lastSensoryPattern }, { lastEffectorPattern, lastEffectorPattern }, { curEffectorPattern, curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
				cv::Mat internalResult = Matrix_Math::FromArmaMat(network.Y[0]->getPredictedResponse());

				CHECK(effectorResult.at<float>(1, 0) == 1.0);
			}
		}
	}
}

TEST_CASE("Minimum Y representation test 2", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 32;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on a single type of input")
		{
			cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
			lastSensoryPattern.at<float>(1) = 1.0f;

			cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
			lastEffectorPattern.at<float>(1, 0) = 1.0f;

			cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
			curEffectorPattern.at<float>(2, 0) = 1.0f;

			network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

			THEN("Display the original pattern and get the next pattern back")
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(1) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(1, 0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>(2, 0) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
				cv::Mat internalResult = Matrix_Math::FromArmaMat(network.Y[0]->getPredictedResponse());

				CHECK(effectorResult.at<float>(2, 0) == 1.0);
			}
		}
	}
}

TEST_CASE("Minimum Y representation test in higher dimensions", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 32;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 100 }, Z_NEURONS_PER_AREA = { 100 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on a single type of input")
		{
			cv::Mat highDimensionalInput = cv::Mat::zeros(10, 10, CV_32F);
			cv::randu(highDimensionalInput, cv::Scalar(0.0), cv::Scalar(1.0));
			cv::Mat originalInput = highDimensionalInput;

			cv::Mat lastEffectorPattern = cv::Mat::zeros(100, 1, CV_32F);
			lastEffectorPattern.at<float>(1, 0) = 1.0f;

			cv::Mat curEffectorPattern = cv::Mat::zeros(100, 1, CV_32F);
			curEffectorPattern.at<float>(2, 0) = 1.0f;

			network.Update({ highDimensionalInput }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

			THEN("Perform an intermediate update to disturb the network")
			{
				cv::randu(highDimensionalInput, cv::Scalar(0.0), cv::Scalar(1.0));

				lastEffectorPattern = cv::Mat::zeros(100, 1, CV_32F);
				lastEffectorPattern.at<float>(3, 0) = 1.0f;

				curEffectorPattern = cv::Mat::zeros(100, 1, CV_32F);
				curEffectorPattern.at<float>(5, 0) = 1.0f;

				network.Update({ highDimensionalInput }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			THEN("Do not display the original result when a random pattern is provided")
			{
				highDimensionalInput = originalInput;

				lastEffectorPattern = cv::Mat::zeros(100, 1, CV_32F);
				lastEffectorPattern.at<float>(1, 0) = 1.0f;

				curEffectorPattern = cv::Mat::zeros(100, 1, CV_32F);
				curEffectorPattern.at<float>(2, 0) = 1.0f;

				network.Update({ highDimensionalInput }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());

				CHECK(std::equal(effectorResult.begin<float>(), effectorResult.end<float>(), curEffectorPattern.begin<float>()));
			}
		}
	}
}

TEST_CASE("Time series pattern test", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 40;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on all possible inputs")
		{
			for (int i = 0; i < X_NEURONS_PER_AREA[0] * 1; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(i % X_NEURONS_PER_AREA[0]) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(i % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			THEN("Display the original pattern and get the next pattern back")
			{
				for (int i = 0; i < X_NEURONS_PER_AREA[0] * 1; i++)
				{
					cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
					lastSensoryPattern.at<float>(i % X_NEURONS_PER_AREA[0]) = 1.0f;

					cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
					lastEffectorPattern.at<float>(i % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

					cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
					curEffectorPattern.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

					network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

					cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
					CHECK(effectorResult.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) == 1.0);
				}
			}
		}
	}
}

TEST_CASE("Eventual extinction test", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 16;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on all possible inputs")
		{
			for (int i = 0; i < X_NEURONS_PER_AREA[0] * 200; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				cv::randu(lastSensoryPattern, cv::Scalar(4), cv::Scalar(4));

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				cv::randu(lastEffectorPattern, cv::Scalar(16), cv::Scalar(1));

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				cv::randu(curEffectorPattern, cv::Scalar(16), cv::Scalar(1));

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

				cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
				bool resultIsNotZero = cv::sum(effectorResult) != cv::Scalar(0.0);
				CHECK(resultIsNotZero);
			}
		}
	}
}

TEST_CASE("Limited resources, time series pattern test", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 32;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on all possible inputs multiple times to simulate limited resources")
		{
			for (int i = 0; i < X_NEURONS_PER_AREA[0] * 4; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(i % X_NEURONS_PER_AREA[0]) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(i % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			THEN("Display the original pattern and get the next pattern back")
			{
				for (int i = 0; i < X_NEURONS_PER_AREA[0] * 1; i++)
				{
					cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
					lastSensoryPattern.at<float>(i % X_NEURONS_PER_AREA[0]) = 1.0f;

					cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
					lastEffectorPattern.at<float>(i % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

					cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
					curEffectorPattern.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

					network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

					cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
					CHECK(effectorResult.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) == 1.0);
				}
			}
		}
	}
}

TEST_CASE("Impractical representation test")
{
	const int pattern_width = 2, pattern_height = 2;
	mat randomMat = mat(28, 28, fill::randu);
	mat minimalPattern = mat(pattern_width, pattern_height, fill::zeros);
	mat unfoldedPattern = mat(28, 28, fill::zeros);

	minimalPattern.at(0) = 1.0f;
	minimalPattern.at(3) = 1.0f;

	for (int i = 0; i < unfoldedPattern.n_rows; i++)
	{
		for (int j = 0; j < unfoldedPattern.n_cols; j++)
		{
			if (unfoldedPattern.in_range(i * pattern_width, j * pattern_height, size(pattern_width, pattern_height)))
				unfoldedPattern.submat(i * pattern_width, j * pattern_height, size(pattern_width, pattern_height)) = minimalPattern;
		}
	}

	randomMat.submat(10, 6, size(2, 2)) = minimalPattern;
	randomMat.submat(20, 6, size(2, 2)) = minimalPattern;

	double result = arma::norm_dot(randomMat, unfoldedPattern);

	mat meanInput = mat(pattern_width, pattern_height, fill::zeros);

	for (int i = 0; i < randomMat.n_rows; i++)
	{
		for (int j = 0; j < randomMat.n_cols; j++)
		{
			if (randomMat.in_range(i * pattern_width, j * pattern_height, size(pattern_width, pattern_height)))
				meanInput += randomMat.submat(i * pattern_width, j * pattern_height, size(pattern_width, pattern_height));
		}
	}

	int totalPatterns = (randomMat.n_rows / pattern_width) * (randomMat.n_cols / pattern_height);

	meanInput /= totalPatterns;

	std::stringstream output;
	output << meanInput << endl;
	FAIL(output.str());
}

TEST_CASE("Load MNIST and test network on a traditional dataset")
{
	string 	filename = "mnist/train-images.idx3-ubyte";
	int number_of_images = 60000;
	int image_size = 28 * 28;

	//read MNIST iamge into Armadillo mat vector
	vector<arma::mat> vec;
	read_Mnist(filename, vec);
	cout << vec.size() << endl;
	cout << vec[0].size() << endl;
	cout << vec[0] << endl;

	filename = "mnist/train-labels.idx1-ubyte";

	//read MNIST label into double vector
	vector<double> labels(number_of_images);
	read_Mnist_Label(filename, labels);
	cout << labels.size() << endl; 

	const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
	const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 5, Y_NEURONS = 50;
	vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 784 }, Z_NEURONS_PER_AREA = { 10 };
	const bool NO_DEBUG_OUTPUT = false;

	DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
	network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

	for (int i = 0; i < vec.size(); i++)
	{
		cv::Mat curEffectorPattern = cv::Mat::zeros(10, 1, CV_32F);
		curEffectorPattern.at<float>(labels[i], 0) = 1.0f;

		network.Update({ Matrix_Math::FromArmaMat(vec[i]) }, { cv::Mat::ones(10, 1, CV_32F) }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
	}

	filename = "mnist/t10k-images.idx3-ubyte";
	number_of_images = 10000;

	//read MNIST iamge into Armadillo mat vector
	vec = vector<arma::mat>();
	read_Mnist(filename, vec);
	cout << vec.size() << endl;
	cout << vec[0].size() << endl;
	cout << vec[0] << endl;

	filename = "mnist/t10k-labels.idx1-ubyte";

	//read MNIST label into double vector
	labels = vector<double>(number_of_images);
	read_Mnist_Label(filename, labels);
	cout << labels.size() << endl;

	for (int i = 0; i < vec.size(); i++)
	{
		cv::Mat curEffectorPattern = cv::Mat::zeros(10, 1, CV_32F);
		curEffectorPattern.at<float>(labels[i], 0) = 1.0f;

		network.Update({ Matrix_Math::FromArmaMat(vec[i]) }, { cv::Mat::ones(10, 1, CV_32F) }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

		cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
		CHECK(effectorResult.at<float>(labels[i], 0) == 1.0);
	}
}

TEST_CASE("Multiple time series pattern test under limited resources", "[AREA]")
{
	GIVEN("A basic developmental network")
	{
		const int UNBIASED_INPUTS = 1, UNBIASED_Y_LAYERS = 1, UNBIASED_MOTOR_AREAS = 1;
		const int NO_X_RESPONSE = 0, MINIMUM_Y_RESPONSE = 1, Y_NEURONS = 40;
		vector<int> MINIMUM_Z_RESPONSE = { 1 }, X_NEURONS_PER_AREA = { 16 }, Z_NEURONS_PER_AREA = { 16 };
		const bool NO_DEBUG_OUTPUT = false;

		DevelopmentalNetwork network = DevelopmentalNetwork(UNBIASED_INPUTS, UNBIASED_Y_LAYERS, UNBIASED_MOTOR_AREAS, DISABLE_MODULATION);
		network.Setup(X_NEURONS_PER_AREA, Y_NEURONS, Z_NEURONS_PER_AREA, NO_X_RESPONSE, MINIMUM_Y_RESPONSE, MINIMUM_Z_RESPONSE, NO_DEBUG_OUTPUT);

		WHEN("We perform updates on all possible inputs in ascending order")
		{
			for (int i = 0; i < X_NEURONS_PER_AREA[0] * 1; i++)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>(i % X_NEURONS_PER_AREA[0]) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>(i % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			THEN("Display the original pattern and get the next pattern back")
			{
				for (int i = 0; i < X_NEURONS_PER_AREA[0] * 1; i++)
				{
					cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
					lastSensoryPattern.at<float>(i % X_NEURONS_PER_AREA[0]) = 1.0f;

					cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
					lastEffectorPattern.at<float>(i % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

					cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
					curEffectorPattern.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

					network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

					cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
					CHECK(effectorResult.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) == 1.0);
				}
			}
		}

		WHEN("We perform updates on all possible inputs in descending order")
		{
			for (int i = (X_NEURONS_PER_AREA[0] - 1) * 30; i >= 0; i--)
			{
				cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
				lastSensoryPattern.at<float>((i + 1) % X_NEURONS_PER_AREA[0]) = 1.0f;

				cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				lastEffectorPattern.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

				cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
				curEffectorPattern.at<float>(i % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

				network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));
			}

			THEN("Display the original pattern and get the next pattern back")
			{
				for (int i = X_NEURONS_PER_AREA[0] - 1; i >= 0; i--)
				{
					cv::Mat lastSensoryPattern = cv::Mat::zeros(4, 4, CV_32F);
					lastSensoryPattern.at<float>((i + 1) % X_NEURONS_PER_AREA[0]) = 1.0f;

					cv::Mat lastEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
					lastEffectorPattern.at<float>((i + 1) % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

					cv::Mat curEffectorPattern = cv::Mat::zeros(16, 1, CV_32F);
					curEffectorPattern.at<float>(i % Z_NEURONS_PER_AREA[0], 0) = 1.0f;

					network.Update({ lastSensoryPattern }, { lastEffectorPattern }, { curEffectorPattern }, false, false, true, cv::Mat::zeros(0, 0, CV_32F), cv::Mat::zeros(0, 0, CV_32F));

					cv::Mat effectorResult = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
					CHECK(effectorResult.at<float>(i % Z_NEURONS_PER_AREA[0], 0) == 1.0);
				}
			}
		}
	}
}