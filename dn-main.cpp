/**
* Dennis Cornwell, updated by Siegfre
* Developmental Network implementation with three modes:
*  - Training: X and Z are supervised, Y provides a perfect link between them
*  - Testing: X is supervised, Z provides esticv::Mated states based on training
*  - Thinking: X is partially supervised and provides esticv::Mates on missing words. Z provides esticv::Mated states based on training
**/

#include "DevelopmentalNetwork.h"
#include "dn-main.h"
#include "image_processor.h"
#include "live_video.h"
#include "vectors.h"
#include <cfloat>
#include <climits>
#include <conio.h>
#include <fstream>
#include <functional>
#include <iomanip>
#include <Math.h>
#include <numeric>
#include <stdio.h>
#include <string>
#include <string.h>
#include <thread>
#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <psapi.h>
#include "Matrix_Math.h"

#define CATCH_CONFIG_RUNNER
#include "catch.hpp" // Our testing framework! - https://github.com/catchorg/Catch2/

#define EPOCHS 6

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
	if (argc == 11)
	{
		DNMain::VideoTest(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]), atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]), atoi(argv[8]),
			atoi(argv[9]), (string)argv[10]);

		return 0;
	}
	
	
	int result = Catch::Session().run(argc, argv);

	return result;
}

HWND FindSaintsRow()
{
	HWND saintsRow = FindWindow(NULL, "Saints Row: The Third");

	if (saintsRow)
		return saintsRow;
	else
		return GetDesktopWindow();
}

// These keys are all accessible to the network, they represent key scan codes, not to be confused with virtual key scan codes.
// www.win.tue.nl/~aeb/linux/kbd/scancodes-1.html
static const map<string, int> KEY_TABLE = { {"1", 0x02 }, { "2", 0x03 }, {"3", 0x04 }, { "4", 0x05 }, { "5", 0x06 }, { "6", 0x07 }, { "7", 0x08 },
{ "8", 0x09 }, { "9", 0x0A }, { "0", 0x0B }, {"q", 0x10 }, { "w", 0x11 }, { "r", 0x13 }, { "t", 0x14 }, { "y", 0x15 }, { "u", 0x16 },
{ "i", 0x17 }, { "o", 0x18 }, { "p", 0x19 }, { "a", 0x1E }, { "s", 0x1F }, { "d", 0x20 }, { "f", 0x21 }, { "g", 0x22 }, { "h", 0x23 }, { "j", 0x24 }, 
{ "k", 0x25 }, { "l", 0x26 }, {"z", 0x2C }, { "x", 0x2D }, { "c", 0x2E }, { "v", 0x2F }, { "d", 0x30 }, { "n", 0x31 }, { "m", 0x32 }, { "space", 0x39 } };

//A list of VK values for every scan code in "KEY_TABLE"
static const map<string, int> SCAN_MAP = { {"1", 0x31},{ "2", 0x32},{ "3", 0x33},{ "4", 0x34},{ "5", 0x35},{ "6", 0x36}, {"7", 0x37},{ "8", 0x38},{ "9", 0x39}, {"0",0x30}, 
{ "q", 0x51},{ "w", 0x57},{ "r", 0x52},{ "t", 0x54},{ "y", 0x59},{ "u", 0x55},{ "i", 0x49},{ "o", 0x4F},{ "p", 0x50},{ "a", 0x41},{ "s", 0x53},{ "d", 0x44},{ "f", 0x46},{ "g", 0x47}, 
{ "h", 0x48},{ "j", 0x4A},{ "k", 0x4B},{ "l", 0x4C},{ "z", 0x5A},{ "x", 0x58}, {"c", 0x43},{ "v", 0x56},{ "d", 0x42},{ "n", 0x4E},{ "m", 0x4D},{ "space", 0x20 } };

// @ represents NULL
static const vector<pair<string, string>> ACTIVE_KEYS = { { "w", "a" },{ "w", "d" },{ "s", "a" },{ "s", "d" },{ "w", "s" },{ "space", "w" }, 
{ "space", "s" }, {"w", "@" }, { "a", "@" }, { "s", "@" }, { "d", "@" },{ "space", "@" }, { "@", "@" } };

void DNMain::updateKeyInput(bool isTraining, int zNeurons, cv::Mat& zInput)
{
	zInput = cv::Mat::zeros(zNeurons, 1, CV_32F);

	if (isTraining)
	{
		for (int i = 0; i < zNeurons; i++)
		{
			if (isKeyPressed(ACTIVE_KEYS[i]))
			{
				zInput.at<float>(i) = 1.0f;
				break;
			}
		}
	}
}

vector<cv::Rect> DNMain::getEyeBounds(double EYE_SIZE, int EYE_COUNT)
{
	int SQR_EYE_COUNT = static_cast<int>(sqrt(EYE_COUNT));
	vector<Rect> eyeBoxes(EYE_COUNT, Rect(0, 0, static_cast<int>(EYE_SIZE), static_cast<int>(EYE_SIZE)));
	for (int e = 0; e < EYE_COUNT; e++)
	{
		eyeBoxes[e].x = fmod(e, SQR_EYE_COUNT) * EYE_SIZE;
		eyeBoxes[e].y = e > 0 ? floor(e / SQR_EYE_COUNT) * EYE_SIZE : 0;
	}
	return eyeBoxes;
}

static const double FPS = 30;
double const UPDATE_RATE = 1000 / FPS;
vector<vector<cv::Mat>> DNMain::getEyeChannels(bool display, int EYE_SIZE, int EYE_COUNT, vector<cv::Rect> eyeBoxes, cv::Mat& videoFrame)
{
	vector<cv::Mat> xEyeInputs = vector<cv::Mat>(EYE_COUNT, cv::Mat::ones(EYE_SIZE, EYE_SIZE, CV_32FC4));

	for (int e = 0; e < eyeBoxes.size(); e++)
		xEyeInputs[e] = cv::Mat(videoFrame, eyeBoxes[e]);

	vector<vector<cv::Mat>> eyeChannels = vector<vector<cv::Mat>>(EYE_COUNT);

	for (int e = 0; e < EYE_COUNT; e++)
	{
		if (display)
			rectangle(videoFrame, eyeBoxes[e], Scalar(255));

		cv::split(xEyeInputs[e], eyeChannels[e]); // RGB Input as separate areas.

		if (eyeChannels[e].empty())
		{
			cout << "NO Eye CHANNELS, SKIPPING!" << endl;
			waitKey(static_cast<int>(UPDATE_RATE));
		}
	}

	return eyeChannels;
}

static bool isTraining = false;
static bool running = true;
static int currentTime = 0;
static INPUT ip;
void DNMain::VideoTest(int xNeurons, int yNeurons, int xRes, int yRes, int  /*rewMult*/, int  /*punMult*/, bool debug, bool block, bool display, const string&  /*name*/)
{
	const int EYE_COUNT = 4;
	const int SQR_EYE_COUNT = static_cast<int>(sqrt(EYE_COUNT));
	const int MODULATORY_INPUTS = 2;
	const int RGB = 3;

	isTraining = block;

	const int sqrResolution = static_cast<int>(sqrt(xNeurons));
	const double EYE_SIZE = floor(sqrResolution / SQR_EYE_COUNT);
	static vector<Rect> eyeBoxes = getEyeBounds(EYE_SIZE, EYE_COUNT);

	const int FULL_EYE_SIZE = static_cast<int>(pow(EYE_SIZE, SQR_EYE_COUNT));
	vector<int> xSizes = { FULL_EYE_SIZE, FULL_EYE_SIZE, FULL_EYE_SIZE,
		FULL_EYE_SIZE, FULL_EYE_SIZE, FULL_EYE_SIZE,
		FULL_EYE_SIZE, FULL_EYE_SIZE, FULL_EYE_SIZE,
		FULL_EYE_SIZE, FULL_EYE_SIZE, FULL_EYE_SIZE,
		MODULATORY_INPUTS, MODULATORY_INPUTS };

	const int zNeurons = ACTIVE_KEYS.size();
	vector<int> zSizes = { zNeurons };
	vector<int> zResponses = { 1 };

	DevelopmentalNetwork network = DevelopmentalNetwork(xSizes.size(), 1, zSizes.size(), DISABLE_MODULATION);
	network.Setup(xSizes, yNeurons, zSizes, xRes, yRes, zResponses, true);
	network.X[xSizes.size() - 2]->augmentation = NEGATIVE;
	network.X[xSizes.size() - 1]->augmentation = POSITIVE;

	cv::Mat videoFrame = live_video::hwnd2mat(FindSaintsRow(), false, sqrResolution, sqrResolution);

	if (display)
	{
		namedWindow("DN Sight", WINDOW_NORMAL); // Create a window for display.
		imshow("DN Sight", videoFrame); // Show our image inside it.
	}

	ip.type = INPUT_KEYBOARD;
	ip.ki.wVk = 0;
	ip.ki.time = 0;
	ip.ki.dwExtraInfo = 0;
		
	for (const auto& it : KEY_TABLE)
	{
		ip.ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP;
		ip.ki.wScan = it.second;
		SendInput(1, &ip, sizeof(INPUT));
	}

	cv::Mat xResponse = cv::Mat::ones(sqrResolution, sqrResolution, CV_32F), zResponse = cv::Mat::ones(zNeurons, 1, CV_32F), zLastResponse = cv::Mat::ones(zNeurons, 1, CV_32F), 
		zLastInput = cv::Mat::ones(zNeurons, 1, CV_32F);
	cv::Mat zInput = cv::Mat::ones(zNeurons, 1, CV_32F), xFullInput = cv::Mat::ones(sqrResolution, sqrResolution, CV_32FC4);
	cv::Mat dopamine = cv::Mat::zeros(10, 10, CV_32F), serotonin = cv::Mat::zeros(10, 10, CV_32F);

	vector<cv::Mat> lastChannels = vector<cv::Mat>(xSizes.size());

	for (int i = 0; i < EYE_COUNT * RGB; i++)
		lastChannels[i] = cv::Mat::ones(EYE_SIZE, EYE_SIZE, CV_32F);

	lastChannels[xSizes.size() - 2] = cv::Mat::ones(MODULATORY_INPUTS, 1, CV_32F);
	lastChannels[xSizes.size() - 1] = cv::Mat::ones(MODULATORY_INPUTS, 1, CV_32F);
	
	srand(time(nullptr));

	cv::Mat painInput = cv::Mat::zeros(2, 1, CV_32F), pleasureInput = cv::Mat::zeros(2, 1, CV_32F);

	double totalConfidence = 0.0;
	double averageConfidence = 1.0;

	while (running)
	{
		if ((GetAsyncKeyState(VK_ESCAPE) & 0x8000) == 0x8000)
			running = false;

		videoFrame = live_video::hwnd2mat(FindSaintsRow(), false, sqrResolution, sqrResolution); // Get raw RGB image.

		vector<vector<cv::Mat>> eyeChannels = getEyeChannels(display, EYE_SIZE, EYE_COUNT, eyeBoxes, videoFrame);

		if (display)
			imshow("DN Sight", videoFrame);

		zInput.copyTo(zLastInput);
		updateKeyInput(isTraining, zNeurons, zInput);

		// Helps determine which key presses that need to cease.
		zResponse.copyTo(zLastResponse);
		vector<cv::Mat> lastResponses = { isTraining ? zLastInput : zLastResponse.clone() };

		bool reward = false, punish = false;
		if (isTraining)
		{
			network.Update(lastChannels, lastResponses, { zInput }, false, false, true,
				cv::Mat::zeros(10, 10, CV_32F), cv::Mat::zeros(10, 10, CV_32F));

			cv::Mat tempMat = Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse());
			zInput.copyTo(zResponse);
			//reward = true;
		}
		else
		{
			network.Update(lastChannels, lastResponses, { cv::Mat::zeros(zNeurons, 1, CV_32F) }, false, true, false,
				cv::Mat::zeros(10, 10, CV_32F), cv::Mat::zeros(10, 10, CV_32F));

			// If we've generated "X" from "Y" get the last responses.
			Matrix_Math::FromArmaMat(network.Z[0]->getPredictedResponse()).copyTo(zResponse);

			//double confidence = network.Y[0]->getConfidence();

			//if (confidence <= averageConfidence)
			//	reward = true;

			//if (confidence >= averageConfidence)
			//	punish = true;

			//totalConfidence += confidence;
			//averageConfidence = totalConfidence / currentTime;
		}

		lastChannels = { eyeChannels[0][0].clone(), eyeChannels[0][1].clone(), eyeChannels[0][2].clone(),
			eyeChannels[1][0].clone(), eyeChannels[1][1].clone(), eyeChannels[1][2].clone(),
			eyeChannels[2][0].clone(), eyeChannels[2][1].clone(), eyeChannels[2][2].clone(),
			eyeChannels[3][0].clone(), eyeChannels[3][1].clone(), eyeChannels[3][2].clone(),
			painInput, pleasureInput
		};

		painInput = cv::Mat::zeros(2, 1, CV_32F);
		pleasureInput = cv::Mat::zeros(2, 1, CV_32F);

		if ((GetAsyncKeyState(VK_UP) & 0x8000) == 0x8000)
			pleasureInput.at<float>(1) = 1.0f;
		else
			pleasureInput.at<float>(0) = 1.0f;

		if ((GetAsyncKeyState(VK_DOWN) & 0x8000) == 0x8000)
			painInput.at<float>(1) = 1.0f;
		else
			painInput.at<float>(0) = 1.0f;

		if (reward)
			pleasureInput.at<float>(1) = 1.0f;

		if (punish)
			painInput.at<float>(1) = 1.0f;

		printDebugInputOutput(debug, zNeurons, zResponse, zInput);

		if ((GetAsyncKeyState(VK_F9) & 0x8000) == 0x8000)
		{
			isTraining = !isTraining;
			zResponse = zLastInput.clone();

			while (_kbhit()) _getch();

			vector<INPUT> inputs;
			ip.ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP;

			for (int i = 0; i < zNeurons; i++)
			{
				if (isRealKey(true, i))
				{
					ip.ki.wScan = KEY_TABLE.at(ACTIVE_KEYS[i].first);
					inputs.push_back(ip);
				}

				if (isRealKey(false, i))
				{
					ip.ki.wScan = KEY_TABLE.at(ACTIVE_KEYS[i].second);
					inputs.push_back(ip);
				}
			}

			SendInput(inputs.size(), inputs.data(), sizeof(INPUT));
			Sleep(250);
		}

		dnControlsKeys(zNeurons, zResponse);

		waitKey(static_cast<int>(UPDATE_RATE));
		currentTime++;
	}                   

	if (display)
		destroyWindow("DN Sight");
}

int DNMain::CalculateSampleSize(const double EYE_SIZE)
{
	int superSquare = EYE_SIZE;
	vector<int> validValues = vector<int>();

	for (int i = superSquare; i > 1; i--)
	{
		if (superSquare % i == 0)
			validValues.push_back(i);
	}

	if (validValues.size() % 2 == 0)
		superSquare = (validValues.size() / 2) + 1;
	else
		superSquare = round(validValues.size() / 2);

	return validValues[superSquare];
}

static cv::Mat lastZResponse;
void DNMain::dnControlsKeys(int zNeurons, cv::Mat &zResponse)
{
	if (!isTraining)
	{
		if (!lastZResponse.empty())
		{
			vector<INPUT> inputs;
			ip.ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP;

			for (int i = 0; i < zNeurons; i++)
			{
				if (isRealKey(true, i))
				{
					ip.ki.wScan = KEY_TABLE.at(ACTIVE_KEYS[i].first);
					inputs.push_back(ip);
				}

				if (isRealKey(false, i))
				{
					ip.ki.wScan = KEY_TABLE.at(ACTIVE_KEYS[i].second);
					inputs.push_back(ip);
				}
			}

			SendInput(inputs.size(), inputs.data(), sizeof(INPUT));
		}

		lastZResponse = zResponse.clone();

		for (int i = 0; i < zNeurons; i++)
		{
			if (lastZResponse.at<float>(i) > 0)
			{
				bool firstKey = false, secondKey = false;

				if (isRealKey(true, i))
					firstKey = true;

				if (isRealKey(false, i))
					secondKey = true;

				if (firstKey && secondKey)
				{
					keybd_event(SCAN_MAP.at(ACTIVE_KEYS[i].first), KEY_TABLE.at(ACTIVE_KEYS[i].first), 0, 0);
					keybd_event(SCAN_MAP.at(ACTIVE_KEYS[i].second), KEY_TABLE.at(ACTIVE_KEYS[i].second), 0, 0);
				}
				else if (firstKey)
					keybd_event(SCAN_MAP.at(ACTIVE_KEYS[i].first), KEY_TABLE.at(ACTIVE_KEYS[i].first), 0, 0);
				else if (secondKey)
					keybd_event(SCAN_MAP.at(ACTIVE_KEYS[i].second), KEY_TABLE.at(ACTIVE_KEYS[i].second), 0, 0);
			}
		}
	}
}

void DNMain::printDebugInputOutput(bool debug, int zNeurons, cv::Mat &zResponse, cv::Mat &zInput)
{
	if (debug)
	{
		cout << "Output: ";
		for (int i = 0; i < zNeurons; i++)
			cout << zResponse.at<float>(i) << " ";
		cout << endl;

		cout << " Input: ";
		for (int i = 0; i < zNeurons; i++)
		{
			if (zInput.at<float>(i) > 0.01)
				cout << 1 << " ";
			else
				cout << 0 << " ";
		}
		cout << endl;
	}
}

bool DNMain::isKeyPressed(pair<string, string> keys)
{
	bool results[] = { true, true };

	if (keys.first != "@")
	{
		if ((GetAsyncKeyState(SCAN_MAP.at(keys.first)) & 0x8000) == 0x8000)
			results[0] = true;
		else
			results[0] = false;
	}

	if (keys.second != "@")
	{
		if ((GetAsyncKeyState(SCAN_MAP.at(keys.second)) & 0x8000) == 0x8000)
			results[1] = true;
		else
			results[1] = false;
	}

	return results[0] && results[1];
}

bool DNMain::isRealKey(bool first, int i)
{
	return (first ? ACTIVE_KEYS[i].first : ACTIVE_KEYS[i].second) != "@";
}

void DNMain::activateKey(bool first, int i, DWORD state, INPUT& ip, vector<INPUT>& inputs)
{
	ip.ki.dwFlags = state;
	ip.ki.wScan = KEY_TABLE.at(first ? ACTIVE_KEYS[i].first : ACTIVE_KEYS[i].second);
	inputs.push_back(ip);
}