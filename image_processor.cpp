﻿#include "image_processor.h"
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <windows.h>
#include "live_video.h"

#define DIRECTORY "D:\\Projects\\DN - Developmental Networks\\DevelopmentalNetwork-General\\x64\\Release\\"

using namespace cv;
using namespace std;

vector<std::string> image_processor::imageList = vector<string>();

void image_processor::load(bool train, string trainingSet)
{
	imageList.clear();

	std::string pattern(DIRECTORY);
	pattern.append((train ? "train\\" : "test\\"));
	pattern.append(trainingSet);
	pattern.append("\\*.jpg");
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE)
	{
		do 
		{ imageList.push_back(data.cFileName);
		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
}

void CalculateCovariance(vector<Mat>& covChannels, vector<std::vector<Mat>>& channelSamples, int i)
{
	Mat cov, mu;
	// Returns a mat the same size as our original, resized mat
	// We also use this as a cheap way to convert things to CV_32F
	cv::calcCovarMatrix(channelSamples[i], cov, mu, CV_COVAR_SCRAMBLED | CV_COVAR_ROWS | CV_COVAR_SCALE, CV_32F);
	covChannels.push_back(cov);
}

bool image_processor::hasLookupGenerated = false;
double* image_processor::lookupTable;
void image_processor::processFrame(Mat &frame, int sqrResolution, int sampleSize)
{
	Size size(sqrResolution, sqrResolution);

	//Break image up into smaller parts
	std::vector<cv::Mat> channels;
	cv::split(frame, channels);

	cv::Size smallSize(sampleSize, sampleSize);
	std::vector<Mat> covChannels;
	cv::Rect rect;

	vector<std::vector<Mat>> channelSamples(4);

	for (int y = 0; y < sqrResolution; y += smallSize.height)
	{
		for (int x = 0; x < sqrResolution; x += smallSize.width)
		{
			rect = cv::Rect(x, y, smallSize.width, smallSize.height);
			channelSamples[0].push_back(channels[0](rect));
			channelSamples[1].push_back(channels[1](rect));
			channelSamples[2].push_back(channels[2](rect));
			channelSamples[3].push_back(channels[2](rect));
		}
	}

	CalculateCovariance(covChannels, channelSamples, 0);
	CalculateCovariance(covChannels, channelSamples, 1);
	CalculateCovariance(covChannels, channelSamples, 2);
	CalculateCovariance(covChannels, channelSamples, 3);

	merge(covChannels, frame);
	resize(frame, frame, size);
}

pair<vector<double>, string> image_processor::process(bool train, std::string trainingSet, int index, bool display)
{
	Size size(RESOLUTION, RESOLUTION);

	pair<vector<double>, string> imageSlice = pair<vector<double>, string>();

	if (index >= imageList.size())
		return imageSlice;

	imageSlice.first = vector<double>(RESOLUTION * RESOLUTION);
	imageSlice.second = imageList[index].substr(0, imageList[index].size() - 6);

	Mat image, resized;
	image = imread((string)DIRECTORY + (train ? "train\\" : "test\\") + trainingSet + "\\" + imageList[index], IMREAD_GRAYSCALE); // Read the file

	if (image.empty()) // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return imageSlice;
	}

	resize(image, resized, size);

	for (int i = 0; i < RESOLUTION; i++)
	{
		for (int j = 0; j < RESOLUTION; j++)
		{
			imageSlice.first[(i * RESOLUTION) + j] = resized.at<uchar>(i, j) / 255.0;
			cout << "\n" << ((i * RESOLUTION) + j) << "\n";
			cout << (double)resized.at<uchar>(i, j) / 255.0 << "\n";
		}
	}

	if (display)
	{
		namedWindow("Final window", WINDOW_NORMAL); // Create a window for display.
		imshow("Final window", image); // Show our image inside it.

		namedWindow("Resized window", WINDOW_NORMAL); // Create a window for display.
		imshow("Resized window", resized); // Show our image inside it.

		waitKey(0); // Wait for a keystroke in the window
	}

	return imageSlice;
}