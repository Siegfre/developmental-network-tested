#pragma once
#include "DevelopmentalNetwork.h"
#include "catch.hpp"

namespace tests
{
	inline bool isEqual(const cv::Mat& a, const cv::Mat& b)
	{
		return cv::countNonZero(a != b) == 0; //https://stackoverflow.com/questions/9905093/how-to-check-whether-two-matrixes-are-identical-in-opencv
	}
}