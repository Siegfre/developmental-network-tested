/*
* Dennis Cornwell
* Helpers for performing specific math and aggregate operations on arrays of data
*/

#ifndef VECTORS_H
#define VECTORS_H

#include <vector>

class Vectors 
{
public:
	static float length(std::vector<float> vec);
	static float length(std::vector<float*> vec);
	static float length(std::vector<float> vec, float p);
	static float spatialMean(std::vector<float> &dst);
	static void multiply(std::vector<float>& a, std::vector<float> b);
	static std::vector<float> multiply_cpy(std::vector<float>& a, std::vector<float>& b);
	static std::vector<float> multiply_cpy(std::vector<float>& a, float b);
	static float CorrelationCoefficient(std::vector<std::vector<float>> inputs);
	static void   norm(std::vector<float> &dst, std::vector<float> src);
	static void   norm(std::vector<float*> &dst, std::vector<float*> src);
	static std::vector<float> norm(std::vector<float> src);
	static void   divide(std::vector<float> &dst, float src);
	static float dot(std::vector<float> a, std::vector<float> b); // The inner product
	static float dot(std::vector<float*> &a, std::vector<float*> &b); // The inner product
	static float dot(std::vector<float*> &a, std::vector<float> &b); // The inner product
	static void   scaleTo255( char *dst, float *src, unsigned size );
	static void   copy(std::vector<float> &dst, std::vector<float> src);
	static void   copy(std::vector<float> &dst, std::vector<unsigned> src);
	static void   print( float* src, unsigned size );
	/*static void nanHunter(std::vector<float> vec);*/
	static float angle(std::vector<float>& x, std::vector<float>& y, float dotProduct);
	template <class T>
	static void fill(std::vector<T> &dst, T value) 
	{
		for (unsigned i = 0; i < dst.size(); i++) 
			dst[i] = value;
	}
};

#endif
