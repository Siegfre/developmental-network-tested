#include "DevelopmentalNetwork.h"
#include "Matrix_Math.h"
#include "GlobalConst.h"

DevelopmentalNetwork::DevelopmentalNetwork(int xAreas, int yAreas, int zAreas, bool enableModulation)
{
	modulation = enableModulation;

	X = vector<area_ptr>(xAreas);
	Xp = vector<area_ptr>(1);
	Xs = vector<area_ptr>(1);
	Y = vector<area_ptr>(yAreas);
	Yp = vector<area_ptr>(1);
	Ys = vector<area_ptr>(1);
	RN = vector<area_ptr>(1);
	VTA = vector<area_ptr>(1);
	Z = vector<area_ptr>(zAreas);
	Zpain = vector<area_ptr>(zAreas);
	Zpleasure = vector<area_ptr>(zAreas);
}

void DevelopmentalNetwork::SetupX()
{
	for (int i = 0; i < X.size(); i++)
		X[i]->Setup(vector<area_ptr>(), Y, vector<area_ptr>());

	if (modulation)
	{
		Xp[0]->Setup(vector<area_ptr>(), Y, vector<area_ptr>());
		Xs[0]->Setup(vector<area_ptr>(), Y, vector<area_ptr>());
	}
}

void DevelopmentalNetwork::SetupY()
{
	if (modulation)
	{
		RN[0]->Setup({ Xp[0] }, vector<area_ptr>(), vector<area_ptr>());
		VTA[0]->Setup({ Xs[0] }, vector<area_ptr>(), vector<area_ptr>());
	}

	for (int i = 0; i < Y.size(); i++)
	{
		Y[i]->Setup(X, Z, Y); // Lateral excitations serving as internal attention, only in "Y". Pg. 241 (NAI)
		
		if (modulation)
			Y[i]->AddPainPleasureSystems(RN[0], VTA[0]);
	}

	if (modulation)
	{
		Yp[0]->Setup({ Xp[0] }, Z, Yp);
		Yp[0]->AddPainPleasureSystems(RN[0], VTA[0]);
		Ys[0]->Setup({ Xs[0] }, Z, Ys);
		Ys[0]->AddPainPleasureSystems(RN[0], VTA[0]);
	}
}

void DevelopmentalNetwork::SetupZ()
{
	vector<area_ptr> yList = vector<area_ptr>(Y);

	if (modulation)
	{
		yList.push_back(Yp[0]);
		yList.push_back(Ys[0]);
	}

	// Z connects to itself - Proof of the Developmental Networks : DN - 2
	for (int i = 0; i < Z.size(); i++)
	{
		Z[i]->Setup(yList, vector<area_ptr>(), vector<area_ptr>()); // Z layer will quickly learn up until all Y neurons ages are > 100
		Zpain[i]->Setup(yList, vector<area_ptr>(), vector<area_ptr>()); // Z layer will quickly learn up until all Y neurons ages are > 100
		Zpleasure[i]->Setup(yList, vector<area_ptr>(), vector<area_ptr>()); // Z layer will quickly learn up until all Y neurons ages are > 100
		Z[i]->pleasureConnection = Zpleasure[i];
		Z[i]->painConnection = Zpain[i];

		if (modulation)
			Z[i]->AddPainPleasureSystems(RN[0], VTA[0]);
	}
}

void DevelopmentalNetwork::Setup(vector<int> xNumNeurons, int yNumNeurons, vector<int> zNumNeurons, int xResSize, int yResSize, vector<int> zResSize, bool debug)
{
	for (int i = 0; i < X.size(); i++)
		X[i] = new Area(xNumNeurons[i], xResSize, SUPERVISED, false, false, true);	
	
	if (modulation)
	{
		Xp[0] = new Area(100, 100, SUPERVISED, false, false, true);
		Xs[0] = new Area(100, 100, SUPERVISED, false, false, true);
	}

	for (int i = 0; i < Y.size(); i++)
		Y[i] = new Area(yNumNeurons, yResSize, TEMPORAL, false, debug, false);
	
	if (modulation)
	{
		Yp[0] = new Area(100, yResSize, TEMPORAL, false, false, false);
		Ys[0] = new Area(100, yResSize, TEMPORAL, false, false, false);

		RN[0] = new Area(100, 100, TEMPORAL, false, false, false);
		VTA[0] = new Area(100, 100, TEMPORAL, false, false, false);
	}

	for (int i = 0; i < Z.size(); i++)
	{
		Z[i] = new Area(zNumNeurons[i], zResSize[i], SUPERVISED, true, false, false);
		Zpain[i] = new Area(zNumNeurons[i], zResSize[i], SUPERVISED, true, false, false);
		Zpleasure[i] = new Area(zNumNeurons[i], zResSize[i], SUPERVISED, true, false, false);
	}

	SetupX();
	SetupY();
	SetupZ();
}

void DevelopmentalNetwork::Modulate()
{
	double averageModulationPleasure = 0.0, averageModulationPain = 0.0;

	averageModulationPleasure += Y[0]->getAverageTotalPleasure();
	averageModulationPain += Y[0]->getAverageTotalPain();

	const double currentPleasure = Y[0]->getTotalPleasure(), currentPain = Y[0]->getTotalPain();

	for (int i = 0; i < Y.size(); i++)
	{
		if (currentPleasure > averageModulationPleasure + Y[i]->getAveragePleasureVariance())
		{
			for (int j = 0; j < Z.size(); j++)
				Z[j]->allowPleasure();
		}

		if (currentPain > averageModulationPain + Y[i]->getAveragePainVariance())
		{
			for (int j = 0; j < Z.size(); j++)
				Z[j]->allowPain();
		}
	}
}

void DevelopmentalNetwork::UpdateRN(mat& painPattern)
{
	Xp[0]->SetSupervisedResponse(painPattern);
	Xp[0]->ComputePreresponse();
	Xp[0]->update(false, false, false);
	RN[0]->ComputePreresponse();
	RN[0]->update(false, false, false);
}

void DevelopmentalNetwork::UpdateVTA(mat& pleasurePattern)
{
	Xs[0]->SetSupervisedResponse(pleasurePattern);
	Xs[0]->ComputePreresponse();
	Xs[0]->update(false, false, false);
	VTA[0]->ComputePreresponse();
	VTA[0]->update(false, false, false);
}

void DevelopmentalNetwork::UpdateY(vector<cv::Mat>& lastSensoryPattern, vector<cv::Mat>& lastEffectorPattern, bool yFrozen)
{
	for (int i = 0; i < X.size(); i++)
		X[i]->SetSupervisedResponse(Matrix_Math::FromCvMat(lastSensoryPattern[i]));

	for (int i = 0; i < Z.size(); i++)
		Z[i]->SetSupervisedResponse(Matrix_Math::FromCvMat(lastEffectorPattern[i]));

	for (int i = 0; i < Y.size(); i++)
	{
		Y[i]->ComputePreresponse();
		Y[i]->update(yFrozen, false, false);
	}

	if (modulation)
	{
		Yp[0]->ComputePreresponse();
		Yp[0]->update(yFrozen, false, false);
		Ys[0]->ComputePreresponse();
		Ys[0]->update(yFrozen, false, false);
	}

	Modulate();
}

void DevelopmentalNetwork::UpdateZ(vector<cv::Mat>& effectorPattern, bool zFrozen, bool zSupervised)
{
	for (int i = 0; i < Z.size(); i++)
		Z[i]->SetSupervisedResponse(Matrix_Math::FromCvMat(effectorPattern[i]));

	if (Y[0]->isNegativeActive())
	{
		for (int i = 0; i < Z.size(); i++)
		{
			Zpain[i]->SetSupervisedResponse(Matrix_Math::FromCvMat(effectorPattern[i]));
			Zpain[i]->ComputePreresponse(false);
			Zpain[i]->update(false, false, true);
		}
	}

	if (Y[0]->isPositiveActive())
	{
		for (int i = 0; i < Z.size(); i++)
		{
			Zpleasure[i]->SetSupervisedResponse(Matrix_Math::FromCvMat(effectorPattern[i]));
			Zpleasure[i]->ComputePreresponse(false);
			Zpleasure[i]->update(false, false, true);
		}
	}

	for (int i = 0; i < Z.size(); i++)
	{
		Z[i]->ComputePreresponse(false);
		Z[i]->update(zFrozen, false, zSupervised);
	}
}

void DevelopmentalNetwork::Update(vector<cv::Mat> lastSensoryPattern, vector<cv::Mat> lastEffectorPattern, vector<cv::Mat> effectorPattern, bool yFrozen, bool zFrozen, bool zSupervised,
	cv::Mat painPattern, cv::Mat pleasurePattern)
{
	if (modulation)
	{
		UpdateRN(Matrix_Math::FromCvMat(painPattern));
		UpdateVTA(Matrix_Math::FromCvMat(pleasurePattern));
	}

	UpdateY(lastSensoryPattern, lastEffectorPattern, yFrozen);
	UpdateZ(effectorPattern, zFrozen, zSupervised);
}