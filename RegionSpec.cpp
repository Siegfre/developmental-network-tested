#include "Region.h"
#include "catch.hpp"

//SCENARIO("Check if a new neuron should be created", "[AREA]")
//{
//	Region testRegion;
//
//	GIVEN("a neuron is at position 0")
//	{
//		GIVEN("its response is greater then the minimum change, and there are neurons remaining to be initialized")
//		{
//			testRegion.setInitializedNeurons(100);
//			testRegion.GetNeuron(99)->response.postSynaptic = 1.00;
//			WHEN("I check if a new neuron is required")
//			{
//				THEN("do not require a new neuron")
//				{
//					CHECK(testRegion.isNewNeuronRequired(99, 0, 10) == false);
//				}
//			}
//		}
//
//		GIVEN("its response is lower then the minimum change, and there are neurons remaining to be initialized")
//		{
//			testRegion.setInitializedNeurons(100);
//			testRegion.GetNeuron(99)->response.postSynaptic = DBL_EPSILON;
//			WHEN("I check if a new neuron is required")
//			{
//				THEN("require a new neuron")
//				{
//					CHECK(testRegion.isNewNeuronRequired(99, 0, 10) == true);
//				}
//			}
//		}
//	}
//
//	GIVEN("a neuron is at a position greater than 0, its response is lower then the minimum change, and there are neurons remaining to be initialized")
//	{
//		testRegion.setInitializedNeurons(100);
//		testRegion.GetNeuron(99)->response.postSynaptic = DBL_EPSILON;
//		WHEN("I check if a new neuron is required")
//		{
//			THEN("do not require a new neuron")
//			{
//				CHECK(testRegion.isNewNeuronRequired(99, 1, 10) == false);
//			}
//		}
//	}
//}