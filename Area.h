#pragma once

#include "GlobalConst.h"
#include "Neuron.h"
#include "Region.h"
#include <set>
#include <vector>
#include <armadillo>

using namespace std;
using namespace arma;

#define NEURON_COMPARATOR pair<double, int>
#define INT_PAIR pair<int, int>
#define area_ptr Area*

struct BaseNeuron
{
	Representation weightDimensions;
	Representation inputDimensions;
	bool zeroize;
};


class Area
{
public:
	Area() = default;
	virtual ~Area() = default;
	/* The number of neurons, the number of back connections, the number of front connections, the response size, the type of area. 
	_startTime may be any positive value.
	_endTime may be any positive value, or -1. -1 will allow for mitosis indefinitely after the start time. */
	Area(unsigned int nNeurons, Representation neuronPatternSizes, unsigned int sResponse, INPUT_TYPE oType = TEMPORAL, bool _zeroize = false, bool _debug = false, bool squared = true);

	void Setup(vector<area_ptr> cBack, vector<area_ptr> cFront, vector<area_ptr> cLateral);
	static double CalculateAreaPreresponse(vector<mat> inputs, vector<mat> weights);
	// unsigned* input - The input contains all of this area's neurons.
	// The input values represent the neurons being activated by the input signal.
	// Non-activated neurons are set to 0, and activated neurons are set to 1.
	// If input is set to "NULL" no input signal will be assumed.
	void ComputePreresponse(bool skip = false);
	void SetSupervisedResponse(mat input);
	void update(bool isFrozen, bool skip, bool supervised);
	mat getPredictedResponse();
	//void Save(std::string filename);
	//void Load(std::string filename);
	area_ptr pleasureConnection;
	area_ptr painConnection;

	int nResponse; // The total number of neurons firing in the top-k competition.
	Representation neuronWeightDimensions; // The maximum width and height of neuron patterns for each areas neuron

	/* Supervised Response
	 In X it contains the raw input vector, raw pain vector, and raw pleasure vector, containing the environmental sensory inputs.
	 In Y it contains the internal mapping between X & Z.
	 In Z it contains the raw input vector, raw pain vector, and raw pleasure vector. */
	mat supervisedResponse;
	mat temporalResponse;

	vector<area_ptr> front = vector<area_ptr>(), back = vector<area_ptr>(), lateral = vector<area_ptr>();

	static bool hasArea(int areaSize);
	static int getAreaCount(vector<int> area_sizes);
	void SetInitializedNeuronsY();
	vector<Region> regionList;
	void AddPainPleasureSystems(area_ptr pain, area_ptr pleasure);
	double CalculatePainValue();
	double CalculatePleasureValue();
	void OptimizeNeuronDistribution();
	void DestroyNeuron();
	void DestroyNeuron(int selected_neuron_index);
	void SetBaseNeuron (Representation inputDimensions, Representation weightDimensions, bool zeroize);
	vector<Neuron> getNeurons();
	AUGMENTATION augmentation = AUGMENTATION::NEUTRAL; // Is this area a modulatory area?
	double getModulation(int i);
	double getAverageTotalPain();
	double getAverageTotalPleasure();
	double getAveragePainVariance();
	double getAveragePleasureVariance();
	double getTotalPleasure();
	double getTotalPain();
	bool isNegativeActive();
	bool isPositiveActive();
	void allowPleasure();
	void allowPain();
	double getConfidence();

protected:
	bool zeroize = false, debug = false;
	INPUT_TYPE oType = TEMPORAL;

	// Update the neuron's weights.
	void UpdateWeights(int iNeuron, double painValue, double pleasureValue);

private:
	BaseNeuron baseNeuron;
	double minimumResponseThreshhold;
	int runningTime = 1;
	int minimumNeuronsAvailableInEachRegion = 0;
	double mitosisModifier = BASE_MITOSIS_MODIFIER;
	int id = -1;
	double totalFiringRatio;
	double negativeTotal = 1.0, positiveTotal = 1.0;
	double totalPainVariance = DBL_EPSILON, totalPleasureVariance = DBL_EPSILON;
	double curPositiveTotal = 0.0, curNegativeTotal = 0.0;
	vector<Neuron> neurons; // Dynamic list of the currently active neurons
	vector<Neuron> painNeurons; // Should be full for collateral triplets
	vector<Neuron> pleasureNeurons; // Should be full for collateral triplets
	area_ptr painArea;
	area_ptr pleasureArea;
	unsigned int nNeurons;
	vector<NEURON_COMPARATOR> topK = std::vector<NEURON_COMPARATOR>(), topX = std::vector<NEURON_COMPARATOR>(),
		topY = std::vector<NEURON_COMPARATOR>(), topZ = std::vector<NEURON_COMPARATOR>();
	vector<vector<NEURON_COMPARATOR> > topXLocal = std::vector<std::vector<NEURON_COMPARATOR> >(), topZLocal = std::vector<std::vector<NEURON_COMPARATOR> >();
	set<int> topResponses = set<int>(); // The index for the currently selected neuron. For this area and region. [neuron]
	set<int> resetList = set<int>(); // The list of neurons that are to be reused
	bool enablePleasure = false, enablePain = false;

	// In-place neuron calculations
	bool isDebugging();
	Neuron CreateNeuron();
	void SortLocal();
	void SynapseMaintenance(int neuronIndex);
	void SetNeuronInputs(int i, vector<mat> &normalizedInputFront, vector<mat> &normalizedInputBack, vector<mat> &normalizedInputLateral);
	vector<mat> NormalizeInputFront();
	vector<mat> NormalizeInputBack();
	vector<mat> NormalizeInputLateral();
	void CalculateMinimumChange();
	void CalculateAge();
	void CalculateRegionAge(int i);
	bool isNewNeuronRequired(int neuron, int pos);
	int InstantiateNewNeuron(RegionType regionType, int locality = -1);
	bool createNewNeuron(int k, int selected_neuron_index, int &first_neuron);
	bool createNewNeuron(REGION_COMBINATIONS regionId, int selectedNeuronIndex, int& first_neuron);
	void ClearAllRegions();
	int CountRegions();
	void SetInitializedNeuronsZ();
	void SetupRegionIds();
	void SetupRegionNeurons();
	void InitializeRegionParameters();
	void SetupRegions();
	vector<mat>* NormalizePainInput();
	vector<mat>* NormalizePleasureInput();
	int CountUninitializedNeurons();
	void CalculatePreresponses();
	void CalculatePreresponses(int i);
	void prescreen();
	void TopK(REGION_COMBINATIONS regionId, int locality);
	int GetMaxResultsPerRegion();
	static double CalculateTotal(double &x, double &y, double &z, double areaCount);
	bool has_remainder(double value) const;
	vector<int> GetCompetingNeurons(RegionType regionType, int locality);
	RegionType GetRegionTypeFromId(REGION_COMBINATIONS regionId);
	void UpdateWeights(int runningTime, int i, bool trim, double painValue, double pleasureValue);
	void UpdateModulationWeights(int runningTime, int i, vector<mat>* noPleasureInput, vector<mat>* noPainInput, bool trim);
	void DestroyYoungestNeuron(REGION_COMBINATIONS regionId);
	void DestroyOldestNeuron(REGION_COMBINATIONS regionId);
	void DestroyRandom();
	static double HarmonicMean(vector<double> values);
	static double GeometricMean(vector<double> values);
	void updateNeuronModulation();
	void calculateFiringValues();
	void applyReward();
	void applyPunishment();
	void applyModulation();
	void decayModulation(int i);
};