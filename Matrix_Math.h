#pragma once
#include <opencv2/core.hpp>
#include <math.h>
#include <stdio.h>
#include <armadillo>

using namespace std;
using namespace arma;

class Matrix_Math
{
public:
	static void Normalize(cv::Mat input, cv::Mat output);
	static void Normalize(mat input, mat& output);
	static mat FromCvMat(cv::Mat input);
	static cv::Mat FromArmaMat(mat input);
	static cv::Mat FlipMatrix(cv::Mat input);
	static mat FlipMatrix(mat input);

	// Convert the vector position to matrix position
	static pair<int, int> getCoords(int i, int cols)
	{
		double x = i / cols, y = i % cols;
		return pair<int, int>(x, y);
	}
};