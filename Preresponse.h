#pragma once
#include <vector>

#define ALL_REGIONS 3.0

using namespace std;

class Response
{
public:
	Response()
	{
		postSynaptic = 1.0;
		preSynaptic = 1.0;
		locality = -1;
	}

	double postSynaptic, preSynaptic;
	int locality; // Used to determine which X input that the pre-synaptic and post-synaptic response belongs

	inline void Clear()
	{
		postSynaptic = 0.0;
		preSynaptic = 0.0;
	}
};