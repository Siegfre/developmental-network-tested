//#include "catch.hpp"
//#include "Area.h"
//#include "Matrix_Math.h"
//
//TEST_CASE("Area is instantiated", "[area]") {
//	Area a = Area(16, 2, INPUT_TYPE::TEMPORAL, true, false);
//	CHECK(a.getTotalNeuronCount() == 0);
//	a.Setup({}, {}, {});
//	CHECK(a.getTotalNeuronCount() == 16);
//	CHECK(a.nResponse == 2);
//	CHECK(a.sqrNeurons == 4);
//}
//
//TEST_CASE("Setup assigns each area appropriately", "[AREA]")
//{
//	GIVEN("A new TEMPORAL area")
//	{
//		Area a(7, 1, TEMPORAL);
//		WHEN("Setup is called with blank front, back, and lateral areas")
//		{
//			a.Setup({}, {}, {});
//			THEN("no areas are assigned")
//			{
//				CHECK(a.front.empty());
//				CHECK(a.back.empty());
//				CHECK(a.lateral.empty());
//			}
//
//			THEN("The area should have no regions")
//			{
//				CHECK(a.GetRegions().size() == 0);
//			}
//		}
//
//		Area front(7, 1, SUPERVISED), back(7, 1, SUPERVISED);
//		WHEN("Setup is called with front, back, and lateral areas")
//		{
//			a.Setup({ &back }, { &a }, { &front });
//
//			THEN("The area should have 8 regions")
//			{
//				CHECK(a.GetRegions().size() == 8);
//			}
//		}
//	}
//}
//
//TEST_CASE("Synapse Maintenance is performed", "[area]") {
//	int matrixSize = 10;
//	cv::Mat result;
//	Area::SynapseMaintenance(cv::Mat::ones(matrixSize, matrixSize, CV_32F), cv::Mat::zeros(matrixSize, matrixSize, CV_32F), result);
//	float resultAsScalar = result.at<float>(0, 0);
//	CHECK(resultAsScalar == 0.0f);
//
//	Area::SynapseMaintenance(cv::Mat::ones(matrixSize, matrixSize, CV_32F), cv::Mat::ones(matrixSize, matrixSize, CV_32F), result);
//	resultAsScalar = result.at<float>(0, 0);
//	CHECK(resultAsScalar == 0.1f);
//
//	Area::SynapseMaintenance(cv::Mat::ones(matrixSize, matrixSize, CV_32F), cv::Mat::ones(matrixSize, matrixSize, CV_32F) * 4, result);
//	resultAsScalar = result.at<float>(0, 0);
//	CHECK(resultAsScalar == 0.1f);
//
//	cv::Mat halfScaled = cv::Mat::ones(matrixSize, matrixSize, CV_32F);
//
//	for (int i = 0; i < (matrixSize * matrixSize) / 2; i++)
//		halfScaled.at<float>(i) /= 2;
//
//	Area::SynapseMaintenance(cv::Mat::ones(matrixSize, matrixSize, CV_32F), halfScaled, result);
//	float firstResultAsScalar = result.at<float>(0, 0);
//	float secondResultAsScalar = result.at<float>(9, 9);
//	CHECK(firstResultAsScalar < secondResultAsScalar);
//}
//
//TEST_CASE("Calculate total pre-response", "[area]") {
//	const int SIGNIFICANT_DECIMALS = 5;
//	double x = 1.0, y = 1.0, z = 1.0;
//	double total = Region::CalculateTotal(x, y, z, 3);
//	CHECK(Neuron::round_decimal(total, SIGNIFICANT_DECIMALS) == 1.0);
//
//	x = 0.5;
//	y = 0.5;
//	z = 0.5;
//	total = Region::CalculateTotal(x, y, z, 3);
//	CHECK(Neuron::round_decimal(total, SIGNIFICANT_DECIMALS) == 0.5);
//
//	x = 0.3;
//	y = 0.0;
//	z = 0.3;
//	total = Region::CalculateTotal(x, y, z, 3);
//	CHECK(Neuron::round_decimal(total, SIGNIFICANT_DECIMALS) == 0.2);
//
//	x = 0.0;
//	y = 0.0;
//	z = 0.0;
//	total = Region::CalculateTotal(x, y, z, 3);
//	CHECK(Neuron::round_decimal(total, SIGNIFICANT_DECIMALS) == 0.0);
//}
//
//TEST_CASE("Convert hasArea to a proper numeric value", "[area]") {
//	int val = static_cast<double>(Area::hasArea(3));
//	CHECK(val == 1);
//
//	val = static_cast<int>(Area::hasArea(0));
//	CHECK(val == 0);
//}
//
//TEST_CASE("L2 Normalization", "[area]")
//{
//	vector<double> values = { 0.0, 1.0, 0.0 };
//	vector<double> output;
//
//	cv::normalize(values, output, 1.0, 0.0, cv::NormTypes::NORM_L2);
//	CHECK(output[1] == 1.0);
//
//	values = { 4.0, 3.0 };
//	double length = cv::norm(values, cv::NormTypes::NORM_L2);
//
//	CHECK(length == 5.0);
//
//	values = { 1.0, 1.0, 5.0 };
//	length = cv::norm(values, cv::NormTypes::NORM_L2);
//
//	CHECK(length != 1.0);
//
//	vector<double> unitVector;
//	length = cv::norm(values, cv::NormTypes::NORM_L2);
//	cv::divide(values, length, unitVector);
//	length = cv::norm(unitVector, cv::NormTypes::NORM_L2);
//	CHECK(length == 1.0);
//}
//
//TEST_CASE("Convert neuron index into matrix coordinates.", "[area]") {
//	SECTION("Test with a square matrix") {
//		cv::Mat coordMat = cv::Mat::zeros(10, 10, CV_32F);
//		pair<int, int> coords = Matrix_Math::getCoords(5, 10);
//		CHECK(coords.first == 0);
//		CHECK(coords.second == 5);
//
//		coords = Matrix_Math::getCoords(50, 10);
//		CHECK(coords.first == 5);
//		CHECK(coords.second == 0);
//	}
//}
//
//TEST_CASE("SetInitializedNeuronsY sets up the proper number of neurons per region", "[AREA]")
//{
//	GIVEN("A TEMPORAL area with a region count equal to 8 and a neuron count of 7")
//	{
//		Area a(7, 1, TEMPORAL);
//		a.Setup(vector<area_ptr>(), vector<area_ptr>(), vector<area_ptr>());
//		WHEN("SetInitializedNeuronsY is called")
//		{
//			a.SetInitializedNeuronsY(7);
//			THEN("there should be one neuron per initialized region")
//			{
//				vector<Region>& regions = a.GetRegions();
//				for (int i = 0; i < UNINITIALIZED; i++)
//					CHECK(regions[i].getTotalNeuronCount() == 1);
//
//				CHECK(regions[UNINITIALIZED].getTotalNeuronCount() == 0);
//			}
//		}
//	}
//
//	GIVEN("A SUPERVISED area with a region count equal to 0")
//	{
//		Area a(7, 1, SUPERVISED);
//		a.Setup(vector<area_ptr>(), vector<area_ptr>(), vector<area_ptr>());
//		WHEN("SetInitializedNeuronsY is called")
//		{
//			a.SetInitializedNeuronsY(7);
//			THEN("there should be 0 regions, with 0 neurons")
//			{
//				vector<Region>& regions = a.GetRegions();
//				CHECK(regions.empty());
//			}
//		}
//	}
//}