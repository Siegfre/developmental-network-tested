#pragma once

#include <vector>;

#define NEURON_COMPARATOR pair<double, int>
struct Layer
{
	int rows, cols, slices; // rows and cols are used for 1d & 2d layers, slices for 3d layers
};

struct Representation
{
	std::vector<Layer> xLayers, yLayers, zLayers;
};