//#include "PatternRecognitionTests.h"
//#include "Matrix_Math.h"
//#include <map>
//
//using namespace tests;
//
//TEST_CASE("Perform classification", "[Pattern Recognition]")
//{
//	GIVEN("a developmental network with plenty resources")
//	{
//		DevelopmentalNetwork network(1, 1, 1);
//		network.Setup({ 9 }, 70, { 10 }, 0, 7, { 1 });
//		WHEN("a unique sensory/effector pattern is passed into it")
//		{
//			cv::Mat somePatternSensory = cv::Mat(3, 3, CV_32F), somePatternEffector = cv::Mat::zeros(10, 1, CV_32F);
//			cv::randu(somePatternSensory, cv::Scalar(0.0), cv::Scalar(1.0));
//			somePatternEffector.at<float>(5, 0) = 1.0f;
//
//			THEN("remember the pattern after a single full update")
//			{
//				network.Update({ somePatternSensory }, { somePatternEffector }, { somePatternEffector }, false, false, true, { NEUTRAL });
//				network.Update({ somePatternSensory }, { somePatternEffector }, { somePatternEffector }, false, false, true, { NEUTRAL });
//
//				// If we've generated "X" from "Y" get the last responses.
//				CHECK(isEqual(network.Z[0]->getPredictedResponse(), somePatternEffector) == true);
//			}
//		}
//	}	
//	
//	GIVEN("a developmental network with constrained resources")
//	{
//		DevelopmentalNetwork network(1, 1, 1);
//		network.Setup({ 9 }, 7, { 10 }, 0, 7, { 1 });
//		WHEN("a unique sensory/effector pattern is passed into it")
//		{
//			cv::Mat somePatternSensory = cv::Mat(3, 3, CV_32F), somePatternEffector = cv::Mat::zeros(10, 1, CV_32F);
//			cv::randu(somePatternSensory, cv::Scalar(0.0), cv::Scalar(1.0));
//			somePatternEffector.at<float>(5, 0) = 1.0f;
//
//			THEN("remember the pattern after more than a single full update")
//			{
//				network.Update({ somePatternSensory }, { somePatternEffector }, { somePatternEffector }, false, false, true, { NEUTRAL });
//				network.Update({ somePatternSensory }, { somePatternEffector }, { somePatternEffector }, false, false, true, { NEUTRAL });
//
//				CHECK(isEqual(network.Z[0]->getPredictedResponse(), somePatternEffector) == false);
//
//				for (int i = 0; i < 1000; i++)
//				{
//					network.Update({ somePatternSensory }, { somePatternEffector }, { somePatternEffector }, false, false, true, { NEUTRAL });
//
//					if (isEqual(network.Z[0]->getPredictedResponse(), somePatternEffector)) {
//						break;
//}
//				}
//
//				// If we've generated "X" from "Y" get the last responses.
//				CHECK(isEqual(network.Z[0]->getPredictedResponse(), somePatternEffector) == true);
//			}
//		}
//	}
//}