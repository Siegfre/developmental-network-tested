#pragma once

#include <Windows.h>
#include <opencv2/core/core.hpp>

using namespace std;
using namespace cv;

class live_video
{
public:
	static cv::Mat hwnd2mat(HWND hwnd, bool gray, int desiredHeight, int desiredWidth);
	static void stop();

private:
	static bool running;
};