#pragma once

#include "GlobalConst.h"
#include "GlobalEnums.h"
#include "GlobalTypes.h"
#include "Neuron.h"
#include <set>
#include <utility>
#include <armadillo>

using namespace arma;

struct neuron_comp
{
	bool operator() (const NEURON_COMPARATOR& lhs, const NEURON_COMPARATOR& rhs) const {
		return lhs.first > rhs.first;
	}
};

struct neuron_intersect
{
	bool comp (const NEURON_COMPARATOR& lhs, const NEURON_COMPARATOR& rhs) const {
		return lhs.second > rhs.second;
	}
};

class Region
{
public:
	~Region() = default;

	Region()
	{
		regionType = RegionType();
		averageAgeOfNeurons = 0;
		totalNeuronsInRegion = 0;
		youngestNeuron = -1;
		oldestNeuron = -1;
		age = 0;
	}

	Region(int regionId) : Region()
	{
		regionType.setRegionId(regionId);
	}

	RegionType regionType;
	int averageAgeOfNeurons;
	int youngestNeuron;
	int oldestNeuron;
	int age;
	int totalNeuronsInRegion;
};