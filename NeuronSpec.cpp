//#include "catch.hpp"
//#include "Neuron.h"
//#include "Matrix_Math.h"
//
//TEST_CASE("Calculate area pre-response match", "[area]") {
//	const int matrixSize = 10;
//	vector<cv::Mat> inputs = vector<cv::Mat>(1, cv::Mat::ones(matrixSize, matrixSize, CV_32F));
//	vector<cv::Mat> weights = vector<cv::Mat>(1, cv::Mat::ones(matrixSize, matrixSize, CV_32F));
//
//	Matrix_Math::Normalize(inputs[0], inputs[0]);
//	Matrix_Math::Normalize(weights[0], weights[0]);
//
//	double result = Neuron::CalculatePreresponse(inputs, weights);
//	CHECK(Neuron::round_decimal(result, 5) == 1.0);
//
//	inputs = vector<cv::Mat>(1, cv::Mat::ones(matrixSize, matrixSize, CV_32F));
//	weights = vector<cv::Mat>(1, cv::Mat::zeros(matrixSize, matrixSize, CV_32F));
//
//	Matrix_Math::Normalize(inputs[0], inputs[0]);
//	Matrix_Math::Normalize(weights[0], weights[0]);
//
//	result = Neuron::CalculatePreresponse(inputs, weights);
//	CHECK(Neuron::round_decimal(result, 5) == 0.0);
//
//	inputs = vector<cv::Mat>(1, cv::Mat::ones(matrixSize, matrixSize, CV_32F));
//	weights = vector<cv::Mat>(1, cv::Mat::ones(matrixSize, matrixSize, CV_32F));
//
//	Matrix_Math::Normalize(inputs[0], inputs[0]);
//	Matrix_Math::Normalize(weights[0], weights[0]);
//
//	for (int i = 0; i < (matrixSize * matrixSize); i++)
//		inputs[0].at<float>(i) /= 2;
//
//	result = Neuron::CalculatePreresponse(inputs, weights);
//	CHECK(Neuron::round_decimal(result, 5) == 0.5);
//}
//
//TEST_CASE("Perform hebbian learning", "[area]") {
//	int matrixSize = 10;
//	cv::Mat weights = cv::Mat::zeros(matrixSize, matrixSize, CV_32F), input = cv::Mat::ones(matrixSize, matrixSize, CV_32F);
//	Neuron::HebbianLearning(weights, 0.0, 1.0, 1.0, input, NEUTRAL);
//	bool matricesEqual = cv::countNonZero(weights != input) == 0;
//	CHECK(matricesEqual);
//
//	weights = cv::Mat::ones(matrixSize, matrixSize, CV_32F) * 3;
//	input = cv::Mat::ones(matrixSize, matrixSize, CV_32F) * 3;
//	Neuron::HebbianLearning(weights, 1.0, 0.0, 1.0, input, NEUTRAL);
//	CHECK(matricesEqual);
//}