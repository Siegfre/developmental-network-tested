#pragma once

#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <string>
#include <vector>

class image_processor
{
public:
	static void load(bool train, std::string trainingSet);
	//Return the image array and category
	static std::pair<std::vector<double>, std::string> process(bool train, std::string trainingSet, int index, bool display = false);
	static void processFrame(cv::Mat &frame, int sqrResolution, int superSquare);

private:
	static std::vector<std::string> imageList;
	static const int RESOLUTION = 32;
	static double* lookupTable;
	static bool hasLookupGenerated;
};