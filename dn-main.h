#pragma once

#include <Windows.h>

class DNMain
{
public:
	static void VideoTest(int xNeurons, int yNeurons, int xRes, int yRes, int rewMult, int punMult,	bool debug = false, bool block = false,
		bool display = false, const string& name = "default");

	static int CalculateSampleSize(const double EYE_SIZE);

	static void dnControlsKeys(int zNeurons, cv::Mat &zResponse);

private:
	static bool isKeyPressed(pair<string, string> keys);
	static bool isRealKey(bool first, int i);
	static void activateKey(bool first, int i, DWORD state, INPUT& ip, vector<INPUT>& inputs);
	static void printDebugInputOutput(bool debug, int zNeurons, cv::Mat &zResponse, cv::Mat &zInput);
	static void updateKeyInput(bool isTraining, int zNeurons, cv::Mat& zInput);
	static vector<cv::Rect> getEyeBounds(double EYE_SIZE, int EYE_COUNT);
	static vector<vector<cv::Mat>> getEyeChannels(bool display, int EYE_SIZE, int EYE_COUNT, vector<cv::Rect> eyeBoxes, cv::Mat& videoFrame);
};