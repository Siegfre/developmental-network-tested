#pragma once

#include "GlobalConst.h"
#include "GlobalTypes.h"
#include "Preresponse.h"
#include "RegionType.h"
#include <armadillo>
#include "Matrix_Math.h"

using namespace arma;

// REFERENCES
// [0] Stereo Where-What Networks: Unsupervised Binocular Feature Learning Mojtaba Solgi Member, IEEE and Juyang Weng, Fellow, IEEE

class Neuron
{
public:
	Neuron() = default;
	virtual ~Neuron() = default;

	Neuron(Layer inputDimensions, Layer weightDimensions, bool zeroize) : Neuron()
	{
		if (zeroize)
			weights = mat(weightDimensions.rows, weightDimensions.cols, fill::zeros);
		else
			weights = mat(weightDimensions.rows, weightDimensions.cols, fill::randu);  // Randomly initialize the matrix if we are not zeroizing it.

		Matrix_Math::Normalize(weights, weights);

		trimmedWeights = mat(weights);
		unfoldedWeights = mat(inputDimensions.rows, inputDimensions.cols, fill::zeros);

		synapticDeviation = mat(weightDimensions.rows, weightDimensions.cols, fill::zeros);
		synapticAges = mat(weightDimensions.rows, weightDimensions.cols, fill::zeros);
		synaptogenicFactor = mat(weightDimensions.rows, weightDimensions.cols, fill::ones);

		input = mat(inputDimensions.rows, inputDimensions.cols, fill::zeros);
		trimmedInput = mat(input);
		input = mat(weightDimensions.rows, weightDimensions.cols, fill::zeros);

		Response response = Response();

		age = 1.0;
		lastFireTime = 1;
		firingRatio = 0.0;
		painSibling = nullptr;
		pleasureSibling = nullptr;
		negativeCount = 0.0;
		positiveCount = 0.0;

		t1 = DEFAULT_T1;
		t2 = DEFAULT_T2;
		SYNAPTOGENIC_TYPE = TYPE_SYNAPTOGENIC_FACTOR::DYNAMIC;
		initialization = INITIAL;
	}

	template <class T>
	static void ClearVector(vector<T>& input)
	{
		input.clear();
	}

	double amnesicMean(double age, int t1, int t2, int c, int r);
	void UpdateWeights(bool trim, double painValue, double pleasureValue);
	static void HebbianLearning(mat& weights, double w1, double w2, double preresponse, const mat &noInput);
	double CalculatePreresponse();
	static double CalculatePreresponse(const mat& inputs, const mat& weights);
	void TrimSynapses();
	static double round_decimal(double input, int places);

	Response response = Response();

	mat weights = mat(), unfoldedWeights = mat(), trimmedWeights = mat(), normalizedWeights = mat();
	mat input, foldedInput, trimmedInput, normalizedInput;
	mat synapticDeviation, synapticAges, synaptogenicFactor; // [0] "Ensures a smooth transition from cutting, scaling, to growing of the synapses."

	double age = 1.0;
	int lastFireTime = 1;
	double firingRatio = 0.0;
	void SetModulationSiblings(Neuron* pain, Neuron* pleasure);
	Neuron *painSibling = nullptr, *pleasureSibling = nullptr;
	bool HasSiblings();
	void Clear();
	double negativeCount = 0, positiveCount = 0;
	INITIALIZATION_STATE initialization = INITIAL; // Neurons begin in an initial state. They maintain that initial state until they find an input pattern that is meaningful.
	void ExpandPattern();
	void CompactPattern();
	RegionType regionType;

private:
	double TrimSynapses(mat &synapticAge, const mat &noInput, const mat &weights, mat &synapticDeviation, mat &compute, mat &trimmedWeights, bool trim);
	double TrimSynapses(mat &synapticAge, const mat &weights, mat &synapticDeviation, mat &synaptogenicFactor, mat &trimmedWeights, bool trim);
	static void SynapseMaintenance(mat &input, const mat &synaptogenicFactor);
	mat ComputeSynaptogenicFactor(mat &synapticAges, mat &synapticDeviation, double averageSynapticVariance, mat synaptogenicFactor);
	void UnfoldPattern(mat &input, mat &output);
	void CompactPattern(mat &input, mat &output);

	// Used in the amnesic mean calculation
	int t1 = DEFAULT_T1, t2 = DEFAULT_T2; // Thresholds for weight change, function changes when neuronal age >= to them.
	TYPE_SYNAPTOGENIC_FACTOR SYNAPTOGENIC_TYPE = TYPE_SYNAPTOGENIC_FACTOR::DYNAMIC;

	void Copy_Internal(const Neuron& neuron)
	{
		response = neuron.response;
		
		weights = neuron.weights;
		trimmedWeights = neuron.trimmedWeights;
		unfoldedWeights = neuron.unfoldedWeights;

		trimmedInput = neuron.trimmedInput;
		input = neuron.input;
		foldedInput = neuron.foldedInput;

		synapticDeviation = neuron.synapticDeviation;
		synapticAges = neuron.synapticAges;
		synaptogenicFactor = neuron.synaptogenicFactor;

		lastFireTime = neuron.lastFireTime;
		firingRatio = neuron.firingRatio;
		initialization = neuron.initialization;

		painSibling = neuron.painSibling;
		pleasureSibling = neuron.pleasureSibling;
		positiveCount = neuron.positiveCount;
		negativeCount = neuron.negativeCount;

		age = neuron.age;
		SYNAPTOGENIC_TYPE = neuron.SYNAPTOGENIC_TYPE;
	}
};