﻿#include "Area.h"
#include "catch.hpp"
#include <algorithm>
#include <cstdlib>
#include <utility>
#include <future>
#include <random>
#include <time.h>
#include <vector>
#include "Matrix_Math.h"
#include <Windows.h>

// REFERENCES
// [0] Neuron-wise Inhibition Zones and Auditory Experiments - DOI 10.1109/TIE.2019.2891400
// [1] Entropy as Temporal Information Density - DOI 10.1109@FUZZ-IEEE.2018.8491505

double Area::CalculateTotal(double &x, double &y, double &z, double areaCount)
{
	const double EFFECTIVELY_ZERO = DBL_EPSILON;
	const double total = x + y + z;

	if (total != 0 && !isnan(total))
		return total / areaCount;
	else
		return EFFECTIVELY_ZERO;
}

bool Area::has_remainder(const double value) const
{
	return abs(fmod(value, 1.0)) <= (DBL_EPSILON * 100);
}

Area::Area(unsigned int nNeurons, Representation neuronPatternSizes, unsigned int sResponse, INPUT_TYPE oType, bool _zeroize, bool _debug, bool squared)
{
	if (nNeurons == 0)
		throw runtime_error("An area is being created with an invalid number of neurons!");	
	
	if (sResponse == 0 && oType == TEMPORAL)
		throw runtime_error("A TEMPORAL area is being created with an invalid response!");

	zeroize = _zeroize;
	this->oType = oType;
	debug = _debug;

	nResponse = sResponse;
	this->nNeurons = nNeurons;
	curPositiveTotal = 0.0;
	curNegativeTotal = 0.0;

	if (this->oType == TEMPORAL)
	{
		const double sqrNeurons = sqrt(this->nNeurons);
		const bool canMake2D = has_remainder(sqrNeurons);

		if (canMake2D)
		{
			supervisedResponse = mat(sqrNeurons, sqrNeurons, fill::ones);
			temporalResponse = mat(sqrNeurons, sqrNeurons, fill::ones);
		}
		else
		{
			supervisedResponse = mat(this->nNeurons, 1, fill::ones);
			temporalResponse = mat(this->nNeurons, 1, fill::ones);
		}
	}
	else
	{
		const double sqrNeurons = sqrt(this->nNeurons);
		const bool canMake2D = has_remainder(sqrNeurons);

		if (squared && canMake2D)
		{
			supervisedResponse = mat(sqrNeurons, sqrNeurons, fill::ones);
			temporalResponse = mat(sqrNeurons, sqrNeurons, fill::ones);
		}
		else
		{
			supervisedResponse = mat(this->nNeurons, 1, fill::ones);
			temporalResponse = mat(this->nNeurons, 1, fill::ones);
		}
	}

	neuronWeightDimensions = neuronPatternSizes;
}

void Area::Setup(vector<area_ptr> cBack, vector<area_ptr> cFront, vector<area_ptr> cLateral)
{
	back = cBack;
	front = cFront;
	lateral = cLateral;

	SetupRegions();
}

void Area::SetupRegions()
{
	InitializeRegionParameters();
	SetupRegionIds();
	SetupRegionNeurons();
}

void Area::InitializeRegionParameters()
{
	minimumResponseThreshhold = (oType == TEMPORAL ? 0.001 : DBL_EPSILON);

	vector<Layer> xLayers = vector<Layer>(back.size()), yLayers = vector<Layer>(lateral.size()), zLayers = vector<Layer>(front.size());

	for (int i = 0; i < xLayers.size(); i++)
	{
		xLayers[i].cols = back[i]->supervisedResponse.n_cols;
		xLayers[i].rows = back[i]->supervisedResponse.n_rows;
	}

	for (int i = 0; i < yLayers.size(); i++)
	{
		yLayers[i].cols = lateral[i]->supervisedResponse.n_cols;
		yLayers[i].rows = lateral[i]->supervisedResponse.n_rows;
	}

	for (int i = 0; i < zLayers.size(); i++)
	{
		zLayers[i].cols = front[i]->supervisedResponse.n_cols;
		zLayers[i].rows = front[i]->supervisedResponse.n_rows;
	}

	Representation neuronInputDimensions = Representation();
	neuronInputDimensions.xLayers = xLayers;
	neuronInputDimensions.yLayers = yLayers;
	neuronInputDimensions.zLayers = zLayers;

	SetBaseNeuron(neuronInputDimensions, neuronWeightDimensions, zeroize);
}

void Area::SetupRegionIds()
{
	if (oType == TEMPORAL)
	{
		for (int i = 0; i < CountRegions(); i++)
			regionList.push_back(Region(i));
	}
	else
	{
		if (!back.empty())
			regionList.push_back(Region(X));

		if (!front.empty())
			regionList.push_back(Region(Z));

		if (!lateral.empty())
			regionList.push_back(Region(Y));
	}
}

void Area::SetBaseNeuron(Representation inputDimensions, Representation weightDimensions, bool zeroize)
{
	baseNeuron.inputDimensions = inputDimensions;
	baseNeuron.weightDimensions = weightDimensions;
	baseNeuron.zeroize = zeroize;
}

void Area::CalculateMinimumChange()
{
	mitosisModifier = 1.0 - DBL_EPSILON; // ((neurons.size() + 1.0) / nNeurons);
}

bool Area::isNewNeuronRequired(int neuron, int pos)
{
	CalculateMinimumChange();

	if (neurons.size() <= neuron)
	{
		cout << "This neuron does not exist, new neuron cannot be ascertained." << endl;
		return false;
	}

	return pos == 0 && neurons[neuron].response.preSynaptic < mitosisModifier;
}

Neuron Area::CreateNeuron()
{
	return Neuron(baseNeuron.inputDimensions, baseNeuron.weightDimensions, baseNeuron.zeroize);
}

int Area::InstantiateNewNeuron(RegionType regionType, int locality)
{
	Neuron *newNeuron;
	int target = -1;
	bool freeNeuronsExist = neurons.size() < nNeurons;

	if (freeNeuronsExist)
	{
		neurons.emplace_back(baseNeuron.inputDimensions, baseNeuron.weightDimensions, baseNeuron.zeroize);
		newNeuron = &neurons.back();
	}
	else // Should only trigger when resetList.size() > 0
	{
		target = *resetList.begin();
		neurons[target] = Neuron(baseNeuron.inputDimensions, baseNeuron.weightDimensions, baseNeuron.zeroize);
		newNeuron = &neurons[target];
	}

	if (newNeuron->HasSiblings())
	{
		newNeuron->painSibling->age = 1;
		newNeuron->pleasureSibling->age = 1;
	}

	newNeuron->SetRegionType(regionType);
	newNeuron->response.locality = locality;
	newNeuron->response.preSynaptic = 0.0;
	newNeuron->response.postSynaptic = 1.0;

	CalculateMinimumChange();

	if (freeNeuronsExist)
		return neurons.size() - 1;

	// Should only trigger when resetList.size() > 0
	resetList.erase(resetList.begin());
	return target;
}

bool Area::createNewNeuron(REGION_COMBINATIONS regionId, int selectedNeuronIndex, int& first_neuron)
{
	if (neurons.size() < nNeurons || !resetList.empty())
	{
		int newNeuronIndex = InstantiateNewNeuron(GetRegionTypeFromId(regionId), neurons[selectedNeuronIndex].response.locality);
		first_neuron = newNeuronIndex;
		return true;
	}

	return false;
}


bool Area::createNewNeuron(int k, int selected_neuron_index, int& first_neuron)
{
	if (isNewNeuronRequired(selected_neuron_index, k))
		return createNewNeuron(neurons[selected_neuron_index].GetRegionType().getRegionId(), selected_neuron_index, first_neuron);

	return false;
}

void Area::DestroyYoungestNeuron(REGION_COMBINATIONS regionId)
{
	int &youngestNeuron = regionList[(int)regionId].youngestNeuron;
	DestroyNeuron(youngestNeuron);
}

void Area::DestroyOldestNeuron(REGION_COMBINATIONS regionId)
{
	int &oldestNeuron = regionList[(int)regionId].oldestNeuron;
	DestroyNeuron(oldestNeuron);
}

void Area::DestroyRandom()
{
	DestroyNeuron(rand() % neurons.size());
}

void Area::SetupRegionNeurons()
{
	neurons.reserve(nNeurons);

	if (oType == TEMPORAL)
	{
		SetInitializedNeuronsY();

		if (CountRegions() == 0)
			return;

		minimumNeuronsAvailableInEachRegion = 3 * nResponse * CountRegions();
	}
	else
		SetInitializedNeuronsZ();
}

void Area::AddPainPleasureSystems(area_ptr pain, area_ptr pleasure)
{
	painArea = pain;
	pleasureArea = pleasure;

	if (oType == SUPERVISED)
	{
		painNeurons.reserve(nNeurons);
		pleasureNeurons.reserve(nNeurons);

		Layer painLayer = Layer(), pleasureLayer = Layer();

		painLayer.rows = pain->supervisedResponse.n_rows;
		painLayer.cols = pain->supervisedResponse.n_cols;

		pleasureLayer.rows = pleasure->supervisedResponse.n_rows;
		pleasureLayer.cols = pleasure->supervisedResponse.n_cols;

		vector<Layer> painLayers = { painLayer }, pleasureLayers = { pleasureLayer }, emptyLayers = { };

		// Pain and Pleasure neurons are designed explicitly for Z with no front or lateral neurons
		for (int i = 0; i < nNeurons; i++)
		{
			painNeurons.emplace_back(painLayers, emptyLayers, emptyLayers, true);
			pleasureNeurons.emplace_back(pleasureLayers, emptyLayers, emptyLayers, true);
			neurons[i].SetModulationSiblings(&painNeurons.back(), &pleasureNeurons.back());
		}
	}
}

double Area::CalculateAreaPreresponse(vector<mat> inputs, vector<mat> weights)
{
	double result = 0.0;
	double similarity = 0;

	for (int f = 0; f < inputs.size(); f++)
	{
		similarity = dot(inputs[f], weights[f]);
		
		if (similarity != 0 && !isnan(similarity))
			result += (similarity / inputs.size());
	}

	return result;
}

int Area::getAreaCount(vector<int> area_sizes)
{
	int total = 0;

	for (int area_size : area_sizes)
		total += area_size;

	return total;
}

bool Area::hasArea(int areaSize)
{
	if (areaSize > 0)
		return true;
	
	return false;
}

vector<mat>* Area::NormalizePainInput()
{
	if (painArea == nullptr || oType == TEMPORAL)
		return nullptr;

	vector<mat> output;

	mat tempMat = painArea->oType == SUPERVISED ? painArea->supervisedResponse : painArea->temporalResponse;
	Matrix_Math::Normalize(tempMat, tempMat);
	output.push_back(tempMat);

	return new vector<mat>(output);
}

vector<mat>* Area::NormalizePleasureInput()
{
	if (pleasureArea == nullptr || oType == TEMPORAL)
		return nullptr;

	vector<mat> output;

	mat tempMat = pleasureArea->oType == SUPERVISED ? pleasureArea->supervisedResponse : pleasureArea->temporalResponse;
	Matrix_Math::Normalize(tempMat, tempMat);
	output.push_back(tempMat);

	return new vector<mat>(output);
}

vector<mat> Area::NormalizeInputFront()
{
	vector<mat> output = vector<mat>(front.size());
	for (int f = 0; f < front.size(); f++)
		Matrix_Math::Normalize(front[f]->oType == SUPERVISED ? front[f]->supervisedResponse : front[f]->temporalResponse, output[f]);
	return output;
}

vector<mat> Area::NormalizeInputBack()
{
	vector<mat> output = vector<mat>(back.size());
	for (int f = 0; f < back.size(); f++)
		Matrix_Math::Normalize(back[f]->oType == SUPERVISED ? back[f]->supervisedResponse : back[f]->temporalResponse, output[f]);
	return output;
}

vector<mat> Area::NormalizeInputLateral()
{
	vector<mat> output = vector<mat>(lateral.size());
	for (int f = 0; f < lateral.size(); f++)
		Matrix_Math::Normalize(lateral[f]->oType == SUPERVISED ? lateral[f]->supervisedResponse : lateral[f]->temporalResponse, output[f]);
	return output;
}

void Area::ClearAllRegions()
{
	topResponses.clear();
	topK.clear();
	topX.clear();
	topXLocal.clear();
	topY.clear();
	topZ.clear();
	topZLocal.clear();

	for (int i = 0; i < regionList.size(); i++)
	{
		regionList[i].age = 0;
		regionList[i].totalNeuronsInRegion = 0;
		regionList[i].youngestNeuron = -1;
		regionList[i].oldestNeuron = -1;
	}
}

void Area::SortLocal()
{
	for (int locality = 0; locality < topXLocal.size(); locality++)
		std::sort(topXLocal[locality].begin(), topXLocal[locality].end(), neuron_comp());

	std::sort(topY.begin(), topY.end(), neuron_comp());

	for (int locality = 0; locality < topZLocal.size(); locality++)
		std::sort(topZLocal[locality].begin(), topZLocal[locality].end(), neuron_comp());
}

void Area::CalculatePreresponses()
{
	for (int i = 0; i < neurons.size(); i++)
		CalculatePreresponses(i);

	SortLocal();
}

void Area::CalculatePreresponses(int i)
{
	neurons[i].Clear();

	double x, y, z, &response = neurons[i].response.preSynaptic;
	double zero = 0.0;
	RegionType curRegionType = neurons[i].GetRegionType();

	if (curRegionType.x)
	{
		// We perform a local top-k on the X inputs if the netework has more than multiple X inputs
		// this ensures that a single Y neuron only learn from and responds to a single X input
		// [0] Section B. Local inhibitions between neurons
		if (back.size() > 1)
			x = neurons[i].CalculateBackPreresponseWithLocalInhibition();
		else
			x = neurons[i].CalculateBackPreresponse();
	}
	else
		x = 0.0;

	if (curRegionType.y)
		y = neurons[i].CalculateLateralPreresponse();
	else
		y = 0.0;

	if (curRegionType.z)
	{
		if (front.size() > 1)
			z = neurons[i].CalculateFrontPreresponseWithLocalInhibition();
		else
			z = neurons[i].CalculateFrontPreresponse();
	}
	else
		z = 0.0;
	
	int regionId = curRegionType.getRegionId();

	if (regionId == X)
	{
		response = x;
	}

	else if (regionId == Y)
	{
		response = y;
	}

	else if (regionId == Z)
	{
		response = z;
	}

	else if (regionId == XYZ)
	{
		response = CalculateTotal(x, y, z, 3.0);
	}

	else if (regionId == XZ)
	{
		const double xz = CalculateTotal(x, zero, z, 2.0);
		response = xz;
	}

	else if (regionId == YZ)
	{
		const double yz = CalculateTotal(zero, y, z, 2.0);
		response = yz;
	}

	else if (regionId == XY)
	{
		const double xy = CalculateTotal(x, y, zero, 2.0);
		response = xy;
	}

	double pleasureResponse = 0.0;
	if (pleasureConnection && enablePleasure)
		pleasureResponse = pleasureConnection->neurons[i].response.preSynaptic;

	double gamma = 1.0;
	double painResponse = 0.0;
	if (painConnection && enablePain)
	{
		painResponse = painConnection->neurons[i].response.preSynaptic;
		gamma = max(1.0 - painResponse, RESPONSE_UNDER_PAIN);
	}

	response = max((response * gamma) * (1.0 + (PLEASURE_MODIFIER * pleasureResponse) - (PAIN_MODIFIER * painResponse)), 0.0);

	if (topXLocal.size() == 0)
		topXLocal = vector<vector<NEURON_COMPARATOR>>(back.size());

	if (topZLocal.size() == 0)
		topZLocal = vector<vector<NEURON_COMPARATOR>>(front.size());

	if (neurons[i].response.locality > -1)
	{
		if (curRegionType.getRegionId() == REGION_COMBINATIONS::X)
			topXLocal[neurons[i].response.locality].push_back(NEURON_COMPARATOR(response, i));
		else if (curRegionType.getRegionId() == REGION_COMBINATIONS::Z)
			topZLocal[neurons[i].response.locality].push_back(NEURON_COMPARATOR(response, i));
	}

	topX.push_back(NEURON_COMPARATOR(x, i));
	topY.push_back(NEURON_COMPARATOR(y, i));
	topZ.push_back(NEURON_COMPARATOR(z, i));
}

bool compare_region(const NEURON_COMPARATOR& lhs, const NEURON_COMPARATOR& rhs)
{
	return lhs.second < rhs.second;
}

double Area::HarmonicMean(vector<double> values)
{
	double totalValues = DBL_EPSILON;

	for (double value : values)
		totalValues += value > 0.0 ? (1.0 / value) : 0.0;

	return values.size() / totalValues;
}

double Area::GeometricMean(vector<double> values)
{
	double totalValues = 1.0;

	for (int i = 0; i < values.size(); i++)
		totalValues *= values[i] > 0.0 ? values[i] : 1.0;

	return pow(totalValues, 1.0 / values.size());
}

void Area::prescreen()
{
	std::sort(topX.begin(), topX.end(), neuron_comp());
	std::sort(topY.begin(), topY.end(), neuron_comp());
	std::sort(topZ.begin(), topZ.end(), neuron_comp());

	if (oType == TEMPORAL)
	{
		int prescreeningMax = 0;
		double prescreeningX = 0, prescreeningY = 0, prescreeningZ = 0;

		for (int i = 0; i < neurons.size(); i++)
		{
			if (topX.size() > i && topX[i].first > 0)
				prescreeningX++;

			if (topY.size() > i && topY[i].first > 0)
				prescreeningY++;

			if (topZ.size() > i && topZ[i].first > 0)
				prescreeningZ++;
		}

		prescreeningMax = max(max(prescreeningX, prescreeningY), prescreeningZ);

		if (prescreeningMax > 0)
			prescreeningMax = min(prescreeningMax, neurons.size());
		else
			return;

		prescreeningMax--;

		double lastX = topX[prescreeningMax].first, lastY = topY[prescreeningMax].first, lastZ = topZ[prescreeningMax].first;

		if (prescreeningMax == 0)
			prescreeningMax = 1;

		if (lastX > 0.0)
			topX.erase(topX.begin() + prescreeningMax, topX.end());

		if (lastY > 0.0)
			topY.erase(topY.begin() + prescreeningMax, topY.end());

		if (lastZ > 0.0)
			topZ.erase(topZ.begin() + prescreeningMax, topZ.end());
	}

	vector<NEURON_COMPARATOR> intersectionOne = vector<NEURON_COMPARATOR>();
	vector<NEURON_COMPARATOR> intersectionTwo = vector<NEURON_COMPARATOR>();

	for (int i = 0; i < topX.size(); i++)
	{
		for (int j = 0; j < topY.size(); j++)
		{
			if (topX[i].second == topY[j].second)
			{
				if (topX[i].first > topY[j].first)
					intersectionOne.push_back(topX[i]);
				else
					intersectionOne.push_back(topY[j]);

				break;
			}
		}
	}

	for (int i = 0; i < intersectionOne.size(); i++)
	{
		for (int j = 0; j < topZ.size(); j++)
		{
			if (intersectionOne[i].second == topZ[j].second)
			{
				if (intersectionOne[i].first > topZ[j].first)
					intersectionTwo.push_back(intersectionOne[i]);
				else
					intersectionTwo.push_back(topZ[j]);

				break;
			}
		}
	}

	if (intersectionTwo.size() < nResponse)
	{
		if (intersectionOne.size() < nResponse)
			topK = topX;
		else
			topK = intersectionOne;
	}
	else
		topK = intersectionTwo;

	for (int i = 0; i < topK.size(); i++)
		topK[i].first = neurons[topK[i].second].response.preSynaptic;

	std::sort(topK.begin(), topK.end(), neuron_comp());
}

void Area::SetNeuronInputs(int i, vector<mat> &normalizedInputFront, vector<mat> &normalizedInputBack, vector<mat> &normalizedInputLateral)
{
	vector<mat> *pleasureInput = NormalizePleasureInput(), *painInput = NormalizePainInput();

	neurons[i].trimmedFrontInput = normalizedInputFront;
	neurons[i].trimmedBackInput = normalizedInputBack;
	neurons[i].trimmedLateralInput = normalizedInputLateral;

	if (neurons[i].HasSiblings())
	{
		if (pleasureInput != nullptr && painInput != nullptr)
		{
			neurons[i].pleasureSibling->trimmedBackInput = *pleasureInput;
			neurons[i].painSibling->trimmedBackInput = *painInput;
		}
		else
			cout << "Pleasure and pain input is not being properly passed in." << endl;
	}
}

void Area::SynapseMaintenance(int i)
{
	neurons[i].TrimSynapses();

	if (neurons[i].HasSiblings())
	{
		neurons[i].painSibling->TrimSynapses();
		neurons[i].pleasureSibling->TrimSynapses();
	}
}

void Area::CalculateAge()
{
	for (int i = 0; i < neurons.size(); i++)
		CalculateRegionAge(i);
}

void Area::CalculateRegionAge(int i)
{
	Neuron &neuron = neurons[i];

	bool isInRegion = neuron.GetRegionType().getRegionId() != UNINITIALIZED;

	if (isInRegion)
	{
		Region &region = oType == TEMPORAL ? regionList[neuron.GetRegionType().getRegionId()] : regionList[0];

		// Used by the area to determine the average age of the region, the average age is used to determine when to destroy neurons
		region.age += neuron.age;
		region.totalNeuronsInRegion++;

		// Used by the area to destroy the youngest neuron
		if (region.youngestNeuron == -1 || neuron.age < neurons[region.youngestNeuron].age)
			region.youngestNeuron = i;

		// Used by the area to destroy the oldest neuron
		if (region.oldestNeuron == -1 || neuron.age > neurons[region.oldestNeuron].age)
			region.oldestNeuron = i;
	}
}

int Area::GetMaxResultsPerRegion()
{
	return max(1, nResponse);
}

vector<int> Area::GetCompetingNeurons(RegionType regionType, int locality)
{
	vector<int> topValues = vector<int>();

	if (regionType.getRegionId() == X)
	{
		for (int i = 0; i < topXLocal[locality].size(); i++)
		{
			if (topXLocal[locality][i].first > MINIMUM_X_FIRING_THRESHOLD)
				topValues.push_back(topXLocal[locality][i].second);
		}
	}

	else if (regionType.getRegionId() == Y)
	{
		for (int i = 0; i < topY.size(); i++)
		{
			if (topY[i].first > MINIMUM_Y_FIRING_THRESHOLD)
				topValues.push_back(topY[i].second);
		}
	}

	else if (regionType.getRegionId() == Z)
	{
		for (int i = 0; i < topZLocal[locality].size(); i++)
		{
			if (topZLocal[locality][i].first > MINIMUM_Z_FIRING_THRESHOLD)
				topValues.push_back(topZLocal[locality][i].second);
		}
	}

	// When neurons are tied for pre-synaptic firing, prioritize recently fired neurons
	for (int i = 0; i < topValues.size(); i++)
	{
		if (i + 1 < topValues.size() && neurons[topValues[i]].response.preSynaptic == neurons[topValues[i + 1]].response.preSynaptic)
		{
			if (neurons[topValues[i]].lastFireTime < neurons[topValues[i + 1]].lastFireTime)
				std::swap(topValues[i], topValues[i + 1]);
		}
	}

	return topValues;
}

RegionType Area::GetRegionTypeFromId(REGION_COMBINATIONS regionId)
{
	RegionType regionType = RegionType();
	regionType.setRegionId(regionId);
	return regionType;
}

void Area::TopK(REGION_COMBINATIONS regionId, int locality)
{
	int maximumResults = GetMaxResultsPerRegion();
	bool isUnsupervised = (oType == TEMPORAL);
	vector<int> competingNeurons = GetCompetingNeurons(GetRegionTypeFromId(regionId), locality); // We add 1 to accomodate the last_fit neuron that we use to produce the adjusted response

	if (!competingNeurons.empty())
	{
		int first_neuron = competingNeurons[0];

		if (isUnsupervised)
		{
			if (createNewNeuron(0, first_neuron, first_neuron))
			{
				if (std::find(competingNeurons.begin(), competingNeurons.end(), first_neuron) == competingNeurons.end())
					competingNeurons.insert(competingNeurons.begin(), first_neuron);
			}
		}

		int last_fit = competingNeurons[competingNeurons.size() - 1];

		vector<pair<int, double>> topNeuronsForRegion = vector<pair<int, double>>();

		const int results = min(maximumResults, competingNeurons.size());

		for (int k = 0; k < results; k++)
		{
			int selected_neuron_index = competingNeurons[k];

			bool responseGreaterThanOrEqualToWeakestResponse = neurons[selected_neuron_index].response.preSynaptic >= neurons[last_fit].response.preSynaptic,
				responseLessThanOrEqualToStrongestResponse = neurons[selected_neuron_index].response.preSynaptic <= neurons[first_neuron].response.preSynaptic;

			/* Adjusted pre-response after top-k competition
			(Mobile Device: Based outdoor Navigation With On-line Learning Neural Network: a Comparison with Convolutional Neural Network) pg. 13
			Best results on pure X testing (r[i] - r[k-1]) / (r[0] - r[k-1]) */
			double adjustedResponse = (neurons[selected_neuron_index].response.preSynaptic - neurons[last_fit].response.preSynaptic) /
				(neurons[first_neuron].response.preSynaptic - neurons[last_fit].response.preSynaptic);

			if (responseGreaterThanOrEqualToWeakestResponse && responseLessThanOrEqualToStrongestResponse && !isnan(adjustedResponse))
				neurons[selected_neuron_index].response.postSynaptic = adjustedResponse;
			else
			{
				if (maximumResults == 1)
					neurons[selected_neuron_index].response.postSynaptic = PERFECT_MATCH;
				else
					neurons[selected_neuron_index].response.postSynaptic = DBL_EPSILON;
			}

			if (topNeuronsForRegion.size() < maximumResults)
				topNeuronsForRegion.emplace_back(std::pair<int, double>(selected_neuron_index, neurons[selected_neuron_index].response.postSynaptic));
			else
				break; // We cannot add anymore neurons, and there are no more neurons that can compete with the current top neurons (as the result of sorting the top-k during prescreening)
		}

		for (int t = 0; t < topNeuronsForRegion.size(); t++)
			topResponses.emplace(topNeuronsForRegion[t].first);
	}
}

void Area::ComputePreresponse(bool skip)
{
	runningTime++;

	if (!skip)
	{
		if (isDebugging())
			cout << "Running time " << runningTime << endl;

		ClearAllRegions();
		calculateFiringValues();
		vector<mat> normalizedInputFront = NormalizeInputFront(), normalizedInputBack = NormalizeInputBack(), normalizedInputLateral = NormalizeInputLateral();
		for (int i = 0; i < neurons.size(); i++)
		{
			SetNeuronInputs(i, normalizedInputFront, normalizedInputBack, normalizedInputLateral);
			SynapseMaintenance(i);
			CalculateRegionAge(i);
		}

		CalculatePreresponses();
		prescreen();

		for (int i = 0; i < back.size(); i++)
			TopK(REGION_COMBINATIONS::X, i);

		if (lateral.size() > 0)
			TopK(REGION_COMBINATIONS::Y, -1);

		for (int i = 0; i < front.size(); i++)
			TopK(REGION_COMBINATIONS::Z, i);

		enablePleasure = false;
		enablePain = false;

		if (isDebugging())
		{
			for (auto neuron = topResponses.begin(); neuron != topResponses.end(); neuron++)
			{
				cout << "Post-Fittings for neuron: " << *neuron << " is " << neurons[*neuron].response.postSynaptic << " age is " << neurons[*neuron].age << endl;
			}
		}
	}
}

void Area::SetSupervisedResponse(mat input)
{
	supervisedResponse = input;
}

void Area::UpdateWeights(int runningTime, int i, bool trim, double painValue, double pleasureValue)
{
	if (neurons.size() <= i)
	{
		cout << "This neuron does not exist, index out of bounds." << endl;
		return;
	}

	neurons[i].UpdateWeights(runningTime, trim, painValue, pleasureValue);
}

void Area::UpdateModulationWeights(int runningTime, int i, vector<mat>* noPleasureInput, vector<mat>* noPainInput, bool trim)
{
	if (neurons.size() <= i)
	{
		cout << "This neuron does not exist, index out of bounds." << endl;
		return;
	}

	if (neurons[i].HasSiblings())
	{
		if (noPleasureInput != nullptr && noPainInput != nullptr)
		{
			neurons[i].pleasureSibling->UpdateWeights(runningTime, noPleasureInput, trim);
			neurons[i].painSibling->UpdateWeights(runningTime, noPainInput, trim);
		}
	}
}

// Update the weight vectors using Hebbian-like learning
void Area::UpdateWeights(int iNeuron, double painValue, double pleasureValue)
{
	neurons[iNeuron].trimmedFrontInput = NormalizeInputFront();
	neurons[iNeuron].trimmedBackInput = NormalizeInputBack();
	neurons[iNeuron].trimmedLateralInput = NormalizeInputLateral();

	UpdateWeights(runningTime, iNeuron, oType == TEMPORAL, painValue, pleasureValue);
	UpdateModulationWeights(runningTime, iNeuron, NormalizePleasureInput(),  NormalizePainInput(), oType == TEMPORAL);
}

// Convert the matrix position to vector position
int getIndex(int x, int y, int size)
{
	return (x * size) + y;
}

double Area::CalculatePainValue()
{
	if (oType == SUPERVISED || painArea == nullptr)
		return 0.0;

	return mean(vectorise(painArea->temporalResponse)) == 1.0 ? 1.0 : 0.0;
}

double Area::CalculatePleasureValue()
{
	if (oType == SUPERVISED || pleasureArea == nullptr)
		return 0.0;

	return  mean(vectorise(pleasureArea->temporalResponse)) == 1.0 ? 1.0 : 0.0;
}

bool Area::isNegativeActive()
{
	for (int i = 0; i < back.size(); i++)
	{
		if (back[i]->augmentation == NEGATIVE && back[i]->supervisedResponse(1) > 0)
			return true;
	}

	return false;
}

bool Area::isPositiveActive()
{
	for (int i = 0; i < back.size(); i++)
	{
		if (back[i]->augmentation == POSITIVE && back[i]->supervisedResponse(1) > 0)
			return true;
	}

	return false;
}

void Area::calculateFiringValues()
{
	totalFiringRatio = 0.0;

	for (int i = 0; i < neurons.size(); i++)
	{
		neurons[i].firingRatio = static_cast<double>(neurons[i].lastFireTime) / static_cast<double>(runningTime);
		totalFiringRatio += neurons[i].firingRatio;
	}
}

double Area::getModulation(int i)
{
	const double likelihoodNeuronFires = neurons[i].firingRatio / totalFiringRatio;
	return neurons.size() * likelihoodNeuronFires;
}

void Area::applyReward()
{
	for (int i = 0; i < neurons.size(); i++)
		neurons[i].positiveCount += getModulation(i);
}

void Area::applyPunishment()
{
	for (int i = 0; i < neurons.size(); i++)
		neurons[i].negativeCount += getModulation(i);
}

void Area::applyModulation()
{
	for (int i = 0; i < neurons.size(); i++)
	{
		const double modulationAmount = getModulation(i);
		neurons[i].positiveCount += modulationAmount;
		neurons[i].negativeCount += modulationAmount;
	}
}

double Area::getAverageTotalPain()
{
	double lastAverage = negativeTotal / runningTime;
	curNegativeTotal = 0.0;

	for (int i = 0; i < neurons.size(); i++)
		curNegativeTotal += neurons[i].negativeCount;

	negativeTotal += curNegativeTotal;

	double curAverage = negativeTotal / runningTime;
	totalPainVariance += (curAverage - lastAverage);

	return curAverage;
}

double Area::getAverageTotalPleasure()
{
	double lastAverage = positiveTotal / runningTime;
	curPositiveTotal = 0.0;

	for (int i = 0; i < neurons.size(); i++)
		curPositiveTotal += neurons[i].positiveCount;

	positiveTotal += curPositiveTotal;

	double curAverage = positiveTotal / runningTime;
	totalPleasureVariance += (curAverage - lastAverage);

	return curAverage;
}

double Area::getTotalPleasure()
{
	return curPositiveTotal;
}

double Area::getTotalPain()
{
	return curNegativeTotal;
}

double Area::getAveragePainVariance()
{
	return totalPainVariance / runningTime;
}

double Area::getAveragePleasureVariance()
{
	return totalPleasureVariance / runningTime;
}

void Area::updateNeuronModulation()
{
	const bool isPositive = isPositiveActive(), isNegative = isNegativeActive();

	if (isPositive && isNegative)
		applyModulation();
	else
	{
		if (isNegative)
			applyPunishment();

		if (isPositive)
			applyReward();
	}
}

void Area::allowPleasure()
{
	enablePleasure = true;
}

void Area::allowPain()
{
	enablePain = true;
}

void Area::decayModulation(int i)
{
	if (!isNegativeActive())
		neurons[i].negativeCount = max(neurons[i].negativeCount - 5, 0);

	if (!isPositiveActive())
		neurons[i].positiveCount = max(neurons[i].positiveCount - 5, 0);
}

// Update the selected neuron with new frequency weights
void Area::update(bool isFrozen, bool skip, bool supervised)
{
	if (skip)
		return;

	temporalResponse.fill(0.0);

	double painValue = CalculatePainValue(), pleasureValue = CalculatePleasureValue();

	for (set<int>::iterator it = topResponses.begin(); it != topResponses.end(); ++it)
	{
		int neuronIndex = *it;
		temporalResponse(neuronIndex) = 1.0; // The generated response, fire.
	}

	updateNeuronModulation();
	
	if (supervised)
	{
		for (int i = 0; i < neurons.size(); i++)
		{
			if (supervisedResponse(i) > 0)
			{
				neurons[i].lastFireTime = runningTime;

				decayModulation(i);

				if (neurons[i].initialization == INITIAL)
				{
					if (neurons[i].response.preSynaptic > SIGNIFICANT_PRESYNAPTIC_ACTIVITY)
					{
						neurons[i].initialization = LEARNING;

						if (resetList.find(i) != resetList.end())
							resetList.erase(resetList.find(i));
					}
					else
						resetList.emplace(i);
				}

				if (!isFrozen)
				{
					neurons[i].response.postSynaptic = supervisedResponse(i);
					UpdateWeights(i, painValue, pleasureValue);
				}
			}
		}
	}
	else
	{
		for (auto it = topResponses.begin(); it != topResponses.end(); ++it)
		{
			neurons[*it].lastFireTime = runningTime;

			decayModulation(*it);

			if (neurons[*it].initialization == INITIAL)
			{
				if (neurons[*it].response.preSynaptic > SIGNIFICANT_PRESYNAPTIC_ACTIVITY)
				{
					neurons[*it].initialization = LEARNING;

					if (resetList.find(*it) != resetList.end())
						resetList.erase(resetList.find(*it));
				}
				else
					resetList.emplace(*it);
			}

			if (!isFrozen)
				UpdateWeights(*it, painValue, pleasureValue);
		}
	}

	if (!isFrozen)
		OptimizeNeuronDistribution();
}

vector<Neuron> Area::getNeurons()
{
	return neurons;
}

mat Area::getPredictedResponse()
{
	return temporalResponse;
}

bool Area::isDebugging()
{
	return debug;
}

int Area::CountUninitializedNeurons()
{
	return nNeurons - neurons.size();
}

void Area::DestroyNeuron(int selected_neuron_index)
{
	if (neurons.size() <= selected_neuron_index)
		return;

	neurons.erase(neurons.begin() + selected_neuron_index);
	CalculateMinimumChange();
}

void Area::DestroyNeuron()
{
	if (neurons.size() == 0)
		return;

	neurons.pop_back();
	CalculateMinimumChange();
}

double Area::getConfidence()
{
	double confidence = 0.0;

	for (int i = 0; i < neurons.size(); i++)
		confidence += neurons[i].response.preSynaptic;

	return (confidence > 0.0 ? confidence / neurons.size() : 0.0);
}

void Area::OptimizeNeuronDistribution()
{
	/*if (oType == SUPERVISED)
		return;

	if (CountUninitializedNeurons() == 0)
		return;


	int averageAgeX = 0, averageAgeY = 0, averageAgeZ = 0;
	int oldestX = 0, oldestY = 0, oldestZ = 0;
	int youngestX = 0, youngestY = 0, youngestZ = 0;

	for (int i = 0; i < topX.size(); i++)
	{
		averageAgeX += neurons[topX[i].second].age;

		if (neurons[topX[i].second].age > neurons[topX[oldestX].second].age)
			oldestX = i;

		if (neurons[topX[i].second].age < neurons[topX[youngestX].second].age)
			youngestX = i;
	}

	for (int i = 0; i < topY.size(); i++)
	{
		averageAgeY += neurons[topY[i].second].age;

		if (neurons[topY[i].second].age > neurons[topY[oldestY].second].age)
			oldestY = i;

		if (neurons[topY[i].second].age < neurons[topY[youngestY].second].age)
			youngestY = i;
	}

	for (int i = 0; i < topZ.size(); i++)
	{
		averageAgeZ += neurons[topZ[i].second].age;

		if (neurons[topZ[i].second].age > neurons[topZ[oldestZ].second].age)
			oldestZ = i;

		if (neurons[topZ[i].second].age < neurons[topZ[youngestZ].second].age)
			youngestZ = i;
	}

	averageAgeX /= topX.size();
	averageAgeY /= topY.size();
	averageAgeZ /= topZ.size();

	int averageRegionAge = (averageAgeX + averageAgeY + averageAgeZ) / (topX.size() + topY.size() + topZ.size());

	int lowerBound = averageRegionAge - (averageRegionAge * 0.5), 
		upperBound = averageRegionAge + (averageRegionAge * 0.5);

	set<int> destructionList = set<int>();

	if (topX.size() > 1)
	{
		if (averageAgeX < lowerBound)
			destructionList.emplace(topX[youngestX].second);
		else if (averageAgeX > upperBound)
			destructionList.emplace(topX[oldestX].second);
	}

	if (topY.size() > 1)
	{
		if (averageAgeY < lowerBound)
			destructionList.emplace(topY[youngestY].second);
		else if (averageAgeY > upperBound)
			destructionList.emplace(topY[oldestY].second);
	}

	if (topZ.size() > 1)
	{
		if (averageAgeZ < lowerBound)
			destructionList.emplace(topZ[youngestZ].second);
		else if (averageAgeZ > upperBound)
			destructionList.emplace(topZ[oldestZ].second);
	}

	for (auto target : destructionList)
	{
		if (neurons[target].age > WAITING_LATENCY_BASIC)
			resetList.emplace(target);
	}*/
}

int Area::CountRegions()
{
	int regionCount = 0;

	if (back.size() > 0 && front.size() > 0 && lateral.size() > 0)
		return ALL_REGION_COMBINATIONS;
	else
	{
		if (back.size() > 0)
			regionCount++;

		if (front.size() > 0)
			regionCount++;

		if (lateral.size() > 0)
			regionCount++;

		if (regionCount == 2)
			regionCount++;
	}

	return regionCount;
}

void Area::SetInitializedNeuronsZ()
{
	for (int i = 0; i < nNeurons; i++)
		InstantiateNewNeuron(GetRegionTypeFromId(REGION_COMBINATIONS::X), 0);
}

void Area::SetInitializedNeuronsY()
{
	// TODO: See if we need to change this to only look for locality and not for all input channels. As is the RGB versions of each locality are treated as their own localities
	for (int i = 0; i < back.size(); i++)
		InstantiateNewNeuron(GetRegionTypeFromId(REGION_COMBINATIONS::X), i);

	InstantiateNewNeuron(GetRegionTypeFromId(REGION_COMBINATIONS::Y), -1);

	for (int i = 0; i < front.size(); i++)
		InstantiateNewNeuron(GetRegionTypeFromId(REGION_COMBINATIONS::Z), i);
}