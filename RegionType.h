#pragma once

#include "GlobalEnums.h"
#include <iostream>

struct RegionType
{
	bool x = false, y = false, z = false;

	// Determines whether or not the regions have overlapping receptive fields
	static bool hasIntersectingReceptiveFields(RegionType a, RegionType b)
	{
		return (a.x == true && b.x == true) || (a.y == true && b.y == true) || (a.z == true && b.z == true);
	}

	REGION_COMBINATIONS getRegionId()
	{
		if (!x && !y && !z)
		{
			//cout << "Invalid region being returned." << endl;
			return UNINITIALIZED;
		}

		if (x && y && z)
			return XYZ;

		if (x && y)
			return XY;

		if (x && z)
			return XZ;

		if (y && z)
			return YZ;

		if (x)
			return X;

		if (y)
			return Y;

		if (z)
			return Z;

		return UNINITIALIZED;
	}

	void setRegionId(int id)
	{
		if (id >= UNINITIALIZED)
		{
			x = false;
			y = false;
			z = false;
			return;
		}

		if (id == XYZ)
		{
			x = true;
			y = true;
			z = true;
			return;
		}

		if (id == XY)
		{
			x = true;
			y = true;
			z = false;
			return;
		}

		if (id == XZ)
		{
			x = true;
			y = false;
			z = true;
			return;
		}

		if (id == YZ)
		{
			x = false;
			y = true;
			z = true;
			return;
		}

		if (id == X)
		{
			x = true;
			y = false;
			z = false;
			return;
		}

		if (id == Y)
		{
			x = false;
			y = true;
			z = false;
			return;
		}

		if (id == Z)
		{
			x = false;
			y = false;
			z = true;
			return;
		}
	}
};