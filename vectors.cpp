#include "vectors.h"
#include <math.h>
#include <cfloat>
#include <stdio.h>
#include <iostream>
#include <complex>

using namespace std;

#define isnan _isnan

// Get the l2 norm
float Vectors::length(std::vector<float> vec) 
{
	float sum = dot(vec, vec);

	return sum <= 0.0 ? 0.0 : pow(sum, 0.5);
}

float Vectors::length(std::vector<float*> vec) 
{
	float sum = dot(vec, vec);

	return sum <= 0.0 ? 0.0 : pow(sum, 0.5);
}

// Get the Lp norm
float Vectors::length(std::vector<float> vec, float p)
{
	float sum = dot(vec, vec);

	return sum <= 0.0 ? 0.0 : pow(sum, 1.0 / p);
}

float Vectors::spatialMean(std::vector<float> &dst)
{
	float result = 0.0;

	for (unsigned i = 0; i < dst.size(); i++)
		result += dst[i];

	return result/dst.size();
}

float Vectors::angle(std::vector<float>& x, std::vector<float>& y, float dotProduct)
{
	return acos(dotProduct/(length(x) * length(y)));
}

/* The inner product of n input vectors. These vectors are converted to have zero spatial mean and a unit length.
The result disregards brightness, and contrast for the comparison. The return value is in the range [-1, 1] */
float Vectors::CorrelationCoefficient(std::vector<std::vector<float>> inputs)
{
	std::vector<float> r(inputs.size()), rSqr(inputs.size());
	for (int i = 0; i < inputs.size(); i++)
	{
		for (int j = 0; j < inputs[i].size(); j++)
		{
			r[i] += (inputs[i][j] - spatialMean(inputs[i]));
			rSqr[i] += pow(inputs[i][j] - spatialMean(inputs[i]), 2);
		}
	}

	float mult = r[0], sqr = rSqr[0];
	for (int i = 1; i < inputs.size(); i++)
	{
		mult *= r[i];
		sqr *= sqrt(rSqr[i]);
	}

	float result = mult / sqr;

	return (isnan(result) ? 0 : result);
}

//std::vector<float> Vectors::CorrelationCoefficient(std::vector<float> &input)
//{
//	float min = input[0], max = input[0];
//
//	for (int j = 0; j < input.size(); j++)
//	{
//		min = input[j] < min ? input[j] : min;
//		max = input[j] > max ? input[j] : max;
//	}
//
//	float diff = max - min + DBL_EPSILON;
//	std::vector<float> r(input.size());
//
//	for (int j = 0; j < input.size(); j++)
//		r[j] = (input[j] - min) / diff;
//
//	float mean = spatialMean(r), norm = 0;
//
//	for (int j = 0; j < input.size(); j++)
//	{
//		r[j] = r[j] - mean + DBL_EPSILON;
//		norm += pow(r[j], 2);
//	}
//
//	norm = sqrt(norm);
//
//	if (norm > 0)
//	{
//		for (int j = 0; j < input.size(); j++)
//			r[j] /= norm;
//	}
//
//	return r;
//}

//Get the unit vector, copy.
std::vector<float> Vectors::norm(std::vector<float> src)
{
	float len = length(src);

	std::vector<float> dst(src.size());

	for (unsigned i = 0; i < src.size(); i++)
		dst[i] = (len > 0 && src[i] != 0 ? src[i] / len : src[i]);

	return dst;
}

//Get the unit vector
void Vectors::norm(std::vector<float> &dst, std::vector<float> src)
{
   float len = length(src);

	for (int i = 0; i < src.size(); i++) 
		dst[i] = (len > 0 && src[i] != 0 ? src[i] / len : src[i]);
}

void Vectors::norm(std::vector<float*> &dst, std::vector<float*> src) 
{
	float len = length(src);

	for (int i = 0; i < src.size(); i++)
		*dst[i] = (len > 0 && *src[i] != 0 ? *src[i] / len : *src[i]);
}

void Vectors::divide(std::vector<float> &dst, float src)
{
	for (unsigned i = 0; i < dst.size(); i++)
	{
		if (dst[i] != 0 && src != 0)
			dst[i] = dst[i] / src;
	}
}

//Preresponse scaling
void Vectors::scaleTo255(char *dst, float *src, unsigned size) 
{
   float max = 0;
   float min = DBL_MAX;

   for ( unsigned i = 0; i < size; i++ ) {
      if ( src[i] > max ) {
         max = src[i];
      }
      if ( src[i] < min ) {
         min = src[i];
      }
   }

   for ( unsigned i = 0; i < size; i++ ) {
      dst[i] = ( 255 * ( (src[i] - min) / (max - min) ) );
   }
}

void Vectors::multiply(std::vector<float>& a, std::vector<float> b)
{
	for (int i = 0; i < a.size(); i++)
		a[i] = a[i] * b[i];
}

std::vector<float> Vectors::multiply_cpy(std::vector<float>& a, std::vector<float>& b)
{
	std::vector<float> c(a);

	for (int i = 0; i < a.size(); i++)
		c[i] = a[i] * b[i];

	return c;
}

std::vector<float> Vectors::multiply_cpy(std::vector<float>& a, float b)
{
	std::vector<float> c(a);

	for (int i = 0; i < a.size(); i++)
		c[i] = a[i] * b;

	return c;
}

float Vectors::dot(std::vector<float> a, std::vector<float> b)
{
   float result = 0.0;

   for (int i = 0; i < a.size(); i++)
      result += a[i] * b[i];

   return result;
}

float Vectors::dot(std::vector<float*> &a, std::vector<float*> &b)
{
   float result = 0.0;

   for (int i = 0; i < a.size(); i++)
      result += *a[i] * *b[i];

   return result;
}

float Vectors::dot(std::vector<float*> &a, std::vector<float> &b)
{
	float result = 0.0;

	for (int i = 0; i < a.size(); i++)
		result += *a[i] * b[i];

	return result;
}

void Vectors::copy(std::vector<float> &dst, std::vector<float> src) {
   for ( unsigned i = 0; i < src.size(); i++ ) {
      dst[i] = src[i];
   }
}

void Vectors::copy(std::vector<float> &dst, std::vector<unsigned> src) {
	for (unsigned i = 0; i < src.size(); i++) {
		dst[i] = src[i];
	}
}

void Vectors::print( float* src, unsigned size ) {
   for ( unsigned i = 0; i < size - 1; i++ ) {
      printf( "%.02f, ", src[i] );
   }
   printf( "%.02f", src[size-1] );
}

//void Vectors::nanHunter(vector<float> vec)
//{
//	for (int i = 0; i < vec.size(); i++)
//	{
//		if (vec[i])
//			throw runtime_error("Len is nan.");
//	}
//}